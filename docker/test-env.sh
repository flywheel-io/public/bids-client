#!/bin/bash
PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/.." && pwd )"

docker run -it --rm \
	-w /local \
	-v "$PROJECT_DIR":/local \
	-e PYTHONPATH=/local \
	flywheel/python:main.771cdc04 /bin/bash -c "pip install -r requirements.txt; pip install -r requirements-dev.txt; /bin/bash"
