# Contributor Guidelines

**bids-client** is a library that provides tooling for the BIDS Specification in
Flywheel.

## Getting Started

**_Clone the bids-client repo:_**

```bash
git clone git@gitlab.com:flywheel-io/public/bids-client.git
cd bids-client
```

**_Synchronize your master branch with the release-candidate branch:_**

```bash
git fetch --all
git checkout release-candidate
```

**_Create a feature branch to hold your development changes, using the
GEAR-##-description-of-changes format:_**

```bash
git checkout -b <GEAR-##-description-of-changes> release-candidate
```

and start making changes. Always use a feature branch from the release-candidate
branch.

**_Record your changes:_**

```bash
git add <files-changed>
git commit -m "<ENH/DOC/FIX/MAIN/REF: changes made.>"
```

**_Push your changes to GitLab:_**

```bash
git push origin <GEAR-##-descriptive-of-changes>
```

and create a merge request for your changes to the release-candidate branch.
Note, all code review occurs on the release-candidate branch.

**_After your MR has been approved, merge your changes to the release-candidate
branch without a fast-forward:_**

```bash
git checkout release-candidate
git merge --no-ff <GEAR-##-descriptive-of-changes> \
    -m "<Merge branch GEAR-## with changes made into release-candidate.>"
git push origin release-candidate
```

**_When the merge is complete and all pipelines are passing, delete your feature
branch:_**

```bash
git push -d origin <GEAR-##-descriptive-of-changes>
git branch -d <GEAR-##-descriptive-of-changes>
```

## Release of bids-client

When a set amount of work has been completed on the release-candidate branch,
for example, all work for an EPIC has been completed, then the release-candidate
branch is tagged. This tagged version is used for testing the downstream
tooling, namely, the curate-bids Gear and the Flywheel CLI.

To tag a new version, on the left-side menu in Gitlab, select `CI/CD` >
`Pipelines`. Click on the blue "Run Pipeline" button on the top-right side.
Under "Run for branch name or tag", enter `release-candidate`, and under
"Variables", enter a new variable: where it says "Input variable key" enter
`RELEASE` (all capitals!) and where it says "Input variable value" enter your
new version (`x.y.z`). Click on "Run Pipeline".

The Gitlab CI then runs the `release:mr` job, which automatically updates the
version in the `pyproject.toml` file, generates a release change-log (from
merges since the last tag) and creates an MR with the changes.

Once you have reviewed that the diff and the change-log looks good on the MR,
you can approve and merge.

At this point, the Gitlab CI will create an annotated tag with the same version
as in the `pyproject.toml`, create a Gitlab release in the repo with the
change-log and will publish a new version of the library to PyPI.

## Example Git Workflow

```markdown
master | |\
| \
| release-candidate | | | |\
| | \
| | GEAR-42-fancy-feature | | / | |/ | |\
| | \
| | GEAR-11-bug-fix | | / | |/ | |> tag rc version X.Y.Z >> test curate-bids
Gear & Flywheel CLI | / | / |> cut release version X.Y.Z |
```
