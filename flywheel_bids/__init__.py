# """Manage all things BIDS with this package."""
from importlib.metadata import version

__version__ = version(__name__)
