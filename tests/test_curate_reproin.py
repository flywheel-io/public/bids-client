import json
import unittest
from pathlib import Path

from flywheel_bids import curate_bids
from flywheel_bids.supporting_files import bidsify_flywheel, project_tree
from flywheel_bids.supporting_files.templates import load_template

ASSETS_DIR = Path(__file__).parent / "assets"

ASSETS_DIR = Path(__file__).parent.resolve() / "assets"


class BidsCurateTestCases(unittest.TestCase):
    def test_intended_for(self):
        """Case 3: Direct field mapping"""

        # set up default reproin.json template
        template = load_template(None, "reproin", True)

        project_node = project_tree.TreeNode("project", {"label": "testProj"})

        session = project_tree.TreeNode(
            "session", {"label": "session1", "subject": {"code": "subj1"}}
        )
        project_node.children.append(session)

        acq1 = project_tree.TreeNode(
            "acquisition",
            {
                "label": "fmap-fieldmap_acq-dwi",
                "created": "2018-01-17T07:58:09.799Z",
                "id": "shmy_d",
            },
        )
        session.children.append(acq1)

        file1 = project_tree.TreeNode(
            "file",
            {
                "name": "24151_9_1.nii.gz",
                "type": "nifti",
                "classification": {"Intent": "Fieldmap"},
            },
        )
        acq1.children.append(file1)

        file2 = project_tree.TreeNode(
            "file",
            {
                "name": "24151_9_1_fieldmap.nii.gz",
                "type": "nifti",
                "classification": {"Intent": "Fieldmap"},
            },
        )
        acq1.children.append(file2)

        acq2 = project_tree.TreeNode(
            "acquisition",
            {
                "label": "fmap-fieldmap",
                "created": "2018-01-17T07:58:09.799Z",
                "id": "shmy_d",
            },
        )
        session.children.append(acq2)

        file3 = project_tree.TreeNode(
            "file",
            {
                "name": "24151_9_1.nii.gz",
                "type": "nifti",
                "classification": {"Intent": "Fieldmap"},
            },
        )
        acq2.children.append(file3)

        file4 = project_tree.TreeNode(
            "file",
            {
                "name": "24151_9_1_fieldmap.nii.gz",
                "type": "nifti",
                "classification": {"Intent": "Fieldmap"},
            },
        )
        acq2.children.append(file4)

        count = curate_bids.Count()

        count = curate_bids.curate_bids_tree(
            None,
            template,
            project_node,
            count,
            reset=True,
            recurate_project=False,
            dry_run=True,
            save_sidecar_as_metadata=True,
        )

        self.assertEqual(count.containers, 3)  # = 1 session + 1 acquisition
        self.assertEqual(count.files, 4)
        self.assertEqual(count.acquisitions, 2)
        self.assertEqual(count.sessions, 1)

        self.assertEqual(file1["info"]["BIDS"]["Path"], "sub-subj1/ses-session1/fmap")
        self.assertEqual(
            file1["info"]["BIDS"]["Filename"],
            "sub-subj1_ses-session1_acq-dwi_magnitude.nii.gz",
        )

        self.assertEqual(file2["info"]["BIDS"]["Path"], "sub-subj1/ses-session1/fmap")
        self.assertEqual(
            file2["info"]["BIDS"]["Filename"],
            "sub-subj1_ses-session1_acq-dwi_fieldmap.nii.gz",
        )

        self.assertEqual(file3["info"]["BIDS"]["Path"], "sub-subj1/ses-session1/fmap")
        self.assertEqual(
            file3["info"]["BIDS"]["Filename"],
            "sub-subj1_ses-session1_magnitude.nii.gz",
        )

        self.assertEqual(file4["info"]["BIDS"]["Path"], "sub-subj1/ses-session1/fmap")
        self.assertEqual(
            file4["info"]["BIDS"]["Filename"],
            "sub-subj1_ses-session1_fieldmap.nii.gz",
        )

    def test_reproin_works(self):
        """Test that the default reproin.json file works on saved ReproIn project data.

        Using the context saved as pickled data that was converted into json files, check that curating
        that context produces the same BIDS Paths, Filenames and IntendedFors as what the context
        had after being successfully curated on GA.
        """

        # Set up default reproin.json template
        template = load_template(None, "reproin", True)

        count = curate_bids.Count()

        # Create context using ReproIn project data converted from pickle files saved when curating that project on ga
        # First, add project data
        with open(ASSETS_DIR / "reproin" / "project.json") as fff:
            project_dict = json.load(fff)

        project_node = project_tree.TreeNode("project", project_dict["data"])

        # add files attached to the project
        for project_children in project_dict["children"]:
            child = project_tree.TreeNode(
                project_children["type"], project_children["data"]
            )
            project_node.children.append(child)  # file

        # Save BIDS info for assertions below (list of acquisitions)
        expected_bids_sessions = []

        # Save IntendedFor lists as the value where the key is the BIDS Path and Filename of the field map
        intended_fors = {}

        # Add 3 sessions with all of their acquisitions and all of the files in those acquisitions
        for session_name in ["session-0.json", "session-1.json", "session-2.json"]:
            with open(ASSETS_DIR / "reproin" / session_name) as fff:
                session_dict = json.load(fff)

            session = project_tree.TreeNode(session_dict["type"], session_dict["data"])

            # Save BIDS info for assertions below (list of files)
            expected_bids_acquisitions = []

            for acquisition_dict in session_dict["children"]:
                sessions_child = project_tree.TreeNode(
                    acquisition_dict["type"], acquisition_dict["data"]
                )
                session.children.append(sessions_child)  # acquisition

                # Save BIDS info for assertions below (BIDS dict for each file)
                expected_bids_files = []

                for file_dict in acquisition_dict["children"]:
                    acquisitions_child = project_tree.TreeNode(
                        file_dict["type"], file_dict["data"]
                    )
                    sessions_child.children.append(acquisitions_child)  # file
                    expected_bids_files.append(file_dict["data"]["info"]["BIDS"])

                    if "IntendedFor" in file_dict["data"]["info"]:
                        field_map_path_and_name = (
                            file_dict["data"]["info"]["BIDS"]["Path"]
                            + "/"
                            + file_dict["data"]["info"]["BIDS"]["Filename"]
                        )
                        intended_fors[field_map_path_and_name] = file_dict["data"][
                            "info"
                        ]["IntendedFor"]

                    if len(file_dict["children"]) > 0:
                        print("What?  Files have children?")

                expected_bids_acquisitions.append(expected_bids_files)

            project_node.children.append(session)

            expected_bids_sessions.append(expected_bids_acquisitions)

        count = curate_bids.curate_bids_tree(
            None,
            template,
            project_node,
            count,
            reset=True,
            recurate_project=True,
            dry_run=False,
            save_sidecar_as_metadata=True,
        )

        # use this code to regenerate the test json files when things change
        # with open('data.json', 'w') as f:
        #    json.dump(project_node.to_json(), f, indent=4)

        self.assertEqual(count.containers, 27)
        self.assertEqual(count.files, 59)
        self.assertEqual(count.acquisitions, 23)
        self.assertEqual(count.sessions, 3)

        # Check BIDS info on project_node container
        self.assertEqual(
            project_node.data["info"]["BIDS"]["Sidecar"],
            "data is in json sidecar, not file.info",
        )
        # Check BIDS info on file on project_node container
        self.assertEqual(project_node.children[0].type, "file")
        self.assertEqual(project_node.children[0].data["info"]["BIDS"], "NA")
        # Check BIDS info on first session container
        self.assertEqual(project_node.children[1].type, "session")
        self.assertEqual(
            project_node.children[1].data["info"]["BIDS"]["Label"], "012220229PM"
        )
        # Check BIDS info on first acquisition container in first session container
        self.assertEqual(project_node.children[1].children[0].type, "acquisition")
        self.assertEqual(
            project_node.children[1].children[0].data["info"]["BIDS"]["rule_id"],
            "bids_acquisition",
        )

        # This demonstrates the kind of comparisons that will be made in the loop below on all of the files
        self.assertEqual(project_node.children[1].children[0].children[0].type, "file")
        self.assertEqual(
            project_node.children[1]
            .children[0]
            .children[0]
            .data["info"]["BIDS"]["Filename"],
            "anat-T1w.zip",
        )
        self.assertEqual(
            project_node.children[1]
            .children[0]
            .children[0]
            .data["info"]["BIDS"]["Path"],
            "sourcedata/sub-10462thwjamesOpenScience/ses-012220229PM",
        )

        # since the project_node was properly curated when the json files were captured, the new curation can be compared to that
        project_sessions = project_node.children[
            1:
        ]  # skip first project_node child which is a file to check only sessions
        for new_ses, bids_ses in zip(project_sessions, expected_bids_sessions):
            for new_acq, bids_acq in zip(new_ses.children, bids_ses):
                for new_file, bids_fil in zip(new_acq.children, bids_acq):
                    if new_file.data["info"]["BIDS"] != bids_fil:
                        print("wait!")
                    self.assertEqual(new_file.data["info"]["BIDS"], bids_fil)
                    if "IntendedFor" in new_file.data["info"]:
                        field_map_path_and_name = (
                            new_file.data["info"]["BIDS"]["Path"]
                            + "/"
                            + new_file.data["info"]["BIDS"]["Filename"]
                        )
                        self.assertEqual(
                            intended_fors[field_map_path_and_name],
                            new_file.data["info"]["IntendedFor"],
                        )

    def test_reproin_dwi_new_way_works(self):
        """Test that the default reproin.json file works on more saved ReproIn project data."""

        # Set up default reproin.json template
        template = load_template(None, "reproin", True)

        count = curate_bids.Count()

        # Create context using ReproIn project data converted from pickle files saved when curating that project on ga
        # First, add project data
        with open(ASSETS_DIR / "reproin" / "project.json") as fff:
            project_dict = json.load(fff)

        project_node = project_tree.TreeNode("project", project_dict["data"])

        # Save BIDS info for assertions below (list of acquisitions)
        expected_bids_sessions = []

        # Add 3 sessions with all of their acquisitions and all of the files in those acquisitions
        with open(ASSETS_DIR / "reproin/session-new.json") as fff:
            session_dict = json.load(fff)

        session = project_tree.TreeNode(session_dict["type"], session_dict["data"])

        # Save BIDS info for assertions below (list of files)
        expected_bids_acquisitions = []

        for acquisition_dict in session_dict["children"]:
            sessions_child = project_tree.TreeNode(
                acquisition_dict["type"], acquisition_dict["data"]
            )
            session.children.append(sessions_child)  # acquisition

            # Save BIDS info for assertions below (BIDS dict for each file)
            expected_bids_files = []

            for file_dict in acquisition_dict["children"]:
                acquisitions_child = project_tree.TreeNode(
                    file_dict["type"], file_dict["data"]
                )
                sessions_child.children.append(acquisitions_child)  # file
                expected_bids_files.append(file_dict["data"]["info"]["BIDS"])

            expected_bids_acquisitions.append(expected_bids_files)

        project_node.children.append(session)

        expected_bids_sessions.append(expected_bids_acquisitions)

        count = curate_bids.curate_bids_tree(
            None,
            template,
            project_node,
            count,
            reset=True,
            recurate_project=True,
            dry_run=False,
            save_sidecar_as_metadata=True,
        )

        # use this code to regenerate the test json files when things change
        # with open('data.json', 'w') as f:
        #    json.dump(project_node.to_json(), f, indent=4)

        self.assertEqual(count.containers, 3)
        self.assertEqual(count.files, 1)
        self.assertEqual(count.acquisitions, 1)
        self.assertEqual(count.sessions, 1)

        self.assertEqual(project_node.children[0].type, "session")
        self.assertEqual(project_node.children[0].children[0].type, "acquisition")
        self.assertEqual(project_node.children[0].children[0].children[0].type, "file")

        self.assertEqual(
            project_node.children[0].children[0].children[0].data["info"]["BIDS"],
            expected_bids_files[0],
        )

    def test_process_matching_templates_anat_t1w(self):
        """ """
        # Define context
        context = {
            "container_type": "file",
            "parent_container_type": "acquisition",
            "project": None,
            "subject": {"code": "001"},
            "session": {
                "label": "sesTEST",
                "info": {"BIDS": {"Label": "sesTEST", "Subject": "001"}},
            },
            "acquisition": {"label": "5 - anat_ses-01_T1w"},
            "file": {
                "classification": {"Measurement": ["T1"], "Intent": "Structural"},
                "type": "nifti",
            },
            "ext": ".nii.gz",
        }

        template = load_template(None, "reproin", True)

        # Call function
        container = bidsify_flywheel.process_matching_templates(context, template)
        # Define expected container
        bids_expected = {
            "template": "anat_file",
            "rule_id": "reproin_anat_file",
            "Filename": "sub-001_ses-sesTEST_T1w.nii.gz",
            "Folder": "anat",
            "Path": "sub-001/ses-sesTEST/anat",
            "Acq": "",
            "Ce": "",
            "Rec": "",
            "Run": "",
            "Inv": "",
            "Mod": "",
            "Echo": "",
            "Part": "",
            "Suffix": "T1w",
            "Custom": "",
            "ignore": False,
        }

        self.assertEqual(container["info"]["BIDS"], bids_expected)

    def test_inversion_time_works(self):
        """BIDS Inv entity"""

        # set up default reproin.json template
        template = load_template(None, "reproin", True)

        project_node = project_tree.TreeNode("project", {"label": "testProj"})

        session = project_tree.TreeNode(
            "session", {"label": "session1", "subject": {"code": "subj1"}}
        )
        project_node.children.append(session)

        acq1 = project_tree.TreeNode(
            "acquisition",
            {
                "label": "anat-mp2rage_inv-1_acq-pTx",
                "created": "2018-01-17T07:58:09.799Z",
                "id": "shmy_d",
            },
        )
        session.children.append(acq1)

        file1 = project_tree.TreeNode(
            "file",
            {
                "name": "t1_mp2rage_sag_pTx.nii.gz",
                "type": "nifti",
                "classification": {"Measurement": ["T1"], "Intent": "Structural"},
            },
        )
        acq1.children.append(file1)

        count = curate_bids.Count()

        count = curate_bids.curate_bids_tree(
            None,
            template,
            project_node,
            count,
            reset=True,
            recurate_project=False,
            dry_run=False,
            save_sidecar_as_metadata=True,
        )

        # print(file1["info"]["BIDS"])

        self.assertEqual(count.containers, 2)  # = 1 session + 1 acquisition
        self.assertEqual(count.files, 1)
        self.assertEqual(count.acquisitions, 1)
        self.assertEqual(count.sessions, 1)

        self.assertEqual(
            file1["info"]["BIDS"],
            {
                "template": "anat_file",
                "rule_id": "reproin_anat_file",
                "Filename": "sub-subj1_ses-session1_acq-pTx_inv-1_mp2rage.nii.gz",
                "Folder": "anat",
                "Path": "sub-subj1/ses-session1/anat",
                "Acq": "pTx",
                "Ce": "",
                "Rec": "",
                "Run": "",
                "Inv": "1",
                "Mod": "",
                "Echo": "",
                "Part": "",
                "Suffix": "mp2rage",
                "Custom": "",
                "ignore": False,
                "valid": True,
                "error_message": "",
            },
        )


if __name__ == "__main__":
    unittest.main()
