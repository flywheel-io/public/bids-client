import pytest

from flywheel_bids.upload_bids import check_bids_dir_name


@pytest.mark.parametrize(
    "test_path, expected_path, expected_foldername",
    [
        (
            "/some/place/in/the/computer/a_big_long-extra-fun-name",
            "/some/place/in/the/computer/a",
            "a",
        ),
        ("/sub/ses/func", "/sub/ses/func", "func"),
    ],
)
def test_check_bids_dir_name_returns_simple_name(
    test_path, expected_path, expected_foldername
):
    result_path, result_foldername = check_bids_dir_name(test_path)
    assert result_foldername == expected_foldername
    assert result_path == expected_path
