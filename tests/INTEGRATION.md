# Integration test workflow

## Dataset

The dataset consists of two subject, each with two sessions.

Includes:

- Main BIDS project-level files (e.g., dataset_description.json)
- Variety of datatypes (anat, dwi, func)
- Run indexing

Future:

- Additional datatypes (fmap)
- Physio & events
- An IntendedFor

## Workflow

1. Run bids-validator with success (no errors) on the dataset.
2. Run the functional equivalent of fw import bids on the dataset.
3. Run the functional equivalent of curate-bids Gear on the imported dataset.
4. Run the functional equivalent of fw export bids on the imported dataset.
5. Run bids-validator on the exported dataset.
6. Run the functional equivalent of fw import bids on the exported dataset.
7. Run the functional equivalent of curate-bids Gear on the re-imported dataset.

## Requirements

You must have the appropriate Flywheel permissions to write across the Flywheel
hierarchy.

## Recommended steps to run

1. Download the repository
2. `cd` in to the repository
3. Build the docker images and open a shell on the testing image by running the
   script:

   `./tests/bin/docker-test.sh --shell`

4. Inside the docker image, run:

   `poetry run python3 /src/tests/test_integration_bids_workflow.py`

(Note: because you are mounting the local (host) repository folder on the Docker
container at `/src`, you can modify the source code in the host and the changes
will be available inside the Docker container. That way you don't need to
re-build the testing Docker image over and over.)

When finished, you may proceed with the instructions in the log.
