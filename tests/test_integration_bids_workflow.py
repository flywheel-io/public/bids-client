"""Flywheel BIDS Workflow Integration Test."""

import logging
import os
import subprocess
import sys
import tempfile
from pathlib import Path

import flywheel

from flywheel_bids.curate_bids import main_with_args
from flywheel_bids.export_bids import export_bids
from flywheel_bids.upload_bids import upload_bids

log = logging.getLogger(__name__)
logging.basicConfig(level=logging.DEBUG)


def run_bids_validator(bids_dataset_dir):
    """Run bids-validator on a local BIDS dataset.

    Args:
        bids_dataset_dir (pathlib.PosixPath): Path to local BIDS dataset.

    Returns:
        is_valid (bool): True only if the bids-validator returns a non-zero
            error code on the input dataset.

    """
    is_valid = False
    log.info("Verifying input directory exists.")
    if not bids_dataset_dir.is_dir():
        log.error(f"{bids_dataset_dir} is not a directory.")

    else:
        log.info("Locating the bids-validator path.")
        process = subprocess.Popen(["which", "bids-validator"], stdout=subprocess.PIPE)
        stdout, stderr = process.communicate()
        bids_validator_path = Path(stdout.decode("utf-8").rstrip("\n"))

        if not bids_validator_path.is_file():
            log.error(
                "Unable to locate bids-validator. "
                "Please install the command line bids-validator via npm "
                "(Reference: https://www.npmjs.com/package/bids-validator)."
            )

        else:
            process = subprocess.Popen(
                ["bids-validator", "--version"], stdout=subprocess.PIPE
            )
            stdout, stderr = process.communicate()
            bids_validator_version = stdout.decode("utf-8").rstrip("\n")

            log.info(
                "Running bids-validator version: "
                f"{bids_validator_version} on input directory."
            )
            process = subprocess.Popen(
                ["bids-validator", str(bids_dataset_dir)],
                stdout=subprocess.PIPE,
                stderr=subprocess.PIPE,
            )
            stdout, stderr = process.communicate()

            bids_validator_output = stdout.decode("utf-8").rstrip("\n")
            log.info(bids_validator_output)

            if process.returncode != 0:
                log.error("Invalid dataset -> bids-validator returned an error.")
            elif process.returncode == 0:
                if "[WARN]" in bids_validator_output:
                    is_valid = True
                    log.warning("Invalid dataset -> bids-validator returned a warning.")
                else:
                    is_valid = True
                    log.debug("Valid dataset")

    return is_valid


def run_flywheel_bids_import(
    api_key, group_id, group_label, project_label, bids_dataset_dir, overwrite=False
):
    """Run the equivalent of Flywheel CLI BIDS Import on a local BIDS dataset.

    Args:
        api_key (str): Flywheel Client API Key
        group_id (str): Flywheel Instance Group ID
        group_label (str): Flywheel Instance Group Label
        project_label (str): Flywheel Instance Project Label
        bids_dataset_dir (pathlib.PosixPath): Path to local BIDS dataset.

    Returns:
        is_imported (bool): True if import was successful.

    """
    is_imported = False
    fw = flywheel.Client(api_key=api_key)

    # Create group and project
    if not fw.groups.find(f"label={group_label}"):
        group_id = fw.add_group(flywheel.Group(group_id, group_label))

    group = fw.get(group_id)

    if group.projects.find(f"label={project_label}"):
        # There is a potential pitfall in having the data already on the instance. I.e, is this test valid.
        # However, to avoid the long upload times, setting is_imported to true will save valuable
        # dev time. (at least while tweaking the tests)
        log.info(f"{project_label} exists. Will not replace.")
        project = fw.projects.find(f"label={project_label}")[0]
        bids_files = [
            f
            for f in os.listdir(bids_dataset_dir)
            if os.path.isfile(os.path.join(bids_dataset_dir, f))
            and not f.startswith(".")
        ]
        if len(bids_files) == len(project["files"]):
            is_imported = True
    else:
        project = group.add_project(label=f"{project_label}")

    if not is_imported or overwrite:
        # Equivalence to fw import bids --project project_label bids_dataset_dir group_id
        upload_bids(
            fw,
            str(bids_dataset_dir),
            group_id=group_id,
            project_label=project.label,
            hierarchy_type="Flywheel",
            validate=False,
            overwrite=overwrite,
            save_sidecar_as_metadata=False,
        )

        is_imported = True

    return is_imported


def run_flywheel_bids_curate(
    api_key, session_id, hard_reset, session_only, template_type
):
    """Run the equivalent of Flywheel CLI BIDS Import on a local BIDS dataset.

    Args:
        api_key (str): Flywheel Client API Key.
        hard_reset (bool): If True, hard delete BIDS metadata before curation.
        session_id (str): Flywheel Instance Session ID.
        session_only (bool): If False, curation run on entire project for
            which the session_id belongs; otherwise, curation run only on
            the session_id indicated.
        bids_dataset_dir (pathlib.PosixPath): Path to local BIDS dataset.

    Returns:
        is_curated (bool): True if curation was successful.

    """
    # Equivalence to running curate-bids Gear
    main_with_args(api_key, session_id, hard_reset, session_only, template_type)
    is_curated = True

    return is_curated


def run_flywheel_bids_export(api_key, group_id, project_label, output_bids_dataset_dir):
    """Run the equivalent of Flywheel CLI BIDS Export on a BIDS curated dataset.

    Args:
        api_key (str): Flywheel Client API Key
        group_id (str): Flywheel Instance Group ID
        project_label (str): Flywheel Instance Project Label
        bids_dataset_dir (pathlib.PosixPath): Path to local directory to export BIDS dataset to.

    Returns:
        is_exported (bool): True if export was successful.

    """

    fw = flywheel.Client(api_key=api_key)

    # Equivalence to fw export bids --project project_label output_bids_dataset_dir
    export_bids(
        fw, str(output_bids_dataset_dir), project_label=project_label, group_id=group_id
    )

    # Returns whether the command populated the test directory
    return any(os.scandir(output_bids_dataset_dir))


def run_flywheel_bids_workflow(
    bids_dataset_dir,
    output_bids_dataset_dir,
    api_key,
    group_id="bids-client",
    group_label="BIDS Integration Testing",
    project_label="bids-client-integration",
):
    """Orchestrate the Flywheel BIDS Workflow. Import, Curate, Export, Validate."""
    if not output_bids_dataset_dir.is_dir():
        log.info(f"Creating {output_bids_dataset_dir}")
        os.mkdir(output_bids_dataset_dir)

    # 1. Run bids-validator with success (yes warnings, no errors) on the dataset
    # !!! ENH: Ideal to have no warnings and no errors - but tools be broke
    log.info("Running bids-validator on the local BIDS dataset.")
    is_valid = run_bids_validator(bids_dataset_dir)

    if not is_valid:
        log.error("%s cannot be validated.", bids_dataset_dir)
        sys.exit(1)
    if is_valid:
        log.info("Running fw import bids equivalent.")
        is_imported = run_flywheel_bids_import(
            api_key=api_key,
            group_id=group_id,
            group_label=group_label,
            project_label=project_label,
            bids_dataset_dir=bids_dataset_dir,
        )

    if is_imported is True:
        log.info("Running curate-bids Gear equivalent.")
        fw = flywheel.Client(api_key=api_key)
        project = fw.lookup(f"{group_id}/{project_label}")
        session_ids = [session.id for session in project.sessions.iter()]
        is_curated = run_flywheel_bids_curate(
            api_key=api_key,
            session_id=session_ids[0],
            hard_reset=True,
            session_only=False,
            template_type="ReproIn",
        )

    if is_curated is True:
        log.info("Running fw export bids equivalent.")
        is_exported = run_flywheel_bids_export(
            api_key=api_key,
            group_id=group_id,
            project_label=project.label,
            output_bids_dataset_dir=output_bids_dataset_dir,
        )

    if is_exported is True:
        log.info("Running bids-validator on the exported dataset.")
        is_valid = run_bids_validator(output_bids_dataset_dir)

    if is_valid is True:
        log.info("Flywheel BIDS Workflow Validated.")

    if is_valid is True:
        log.info("Running second fw import bids equivalent on exported directory.")
        is_imported2 = run_flywheel_bids_import(
            api_key=api_key,
            group_id=group_id,
            group_label=group_label,
            project_label=project_label,
            bids_dataset_dir=output_bids_dataset_dir,
            overwrite=False,
        )

    if is_imported2 is True:
        log.info("Running curate-bids Gear equivalent on re-imported dataset.")
        fw = flywheel.Client(api_key=api_key)
        project = fw.lookup(f"{group_id}/{project_label}")
        session_ids = [session.id for session in project.sessions.iter()]
        is_curated2 = run_flywheel_bids_curate(
            api_key=api_key,
            session_id=session_ids[0],
            hard_reset=True,
            session_only=False,
            template_type="ReproIn",
        )

    if is_curated2:
        log.info("Able to re-curate the re-imported dataset.")


def main():
    assets_path = Path(__file__).parent / "assets"
    log.info(f"Assets path: {assets_path}")
    bids_dataset_dir = assets_path / "dataset"
    output_bids_dataset_dir = tempfile.TemporaryDirectory()
    # Since the file is running in a Docker container, the local variables are not present.
    # To solve, add --env-file path_to_hidden_file_with_key (https://stackoverflow.com/questions/30494050/how-do-i-pass-environment-variables-to-docker-containers)
    API_KEY = os.environ.get("FW_KEY")
    GROUP_ID = "bids-client"
    GROUP_LABEL = "BIDS Integration Testing"
    PROJECT_LABEL = "bids-client-integration"

    log.info("Execute Flywheel BIDS Workflow Integration Test.")
    run_flywheel_bids_workflow(
        bids_dataset_dir,
        Path(output_bids_dataset_dir.name),
        api_key=API_KEY,
        group_id=GROUP_ID,
        group_label=GROUP_LABEL,
        project_label=PROJECT_LABEL,
    )

    log.info("Cleaning up temporary directory.")
    output_bids_dataset_dir.cleanup()
    log.info("** Dance Party **")

    return 0


if __name__ == "__main__":
    exit_status = main()
