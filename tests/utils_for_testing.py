import json
from pathlib import Path


def check_for_fw_key(instances):
    """Check for FW's API key in $HOME/.config/flywheel/user.json.

    Check that there is a $HOME/.config/flywheel/user.json file, and that it
    contains a "key" entry (for FW's API). If not found, return an error message.
    This should be called like:

        err = check_for_fw_key("latest.")
        if err != "ok":
            unittest.TestCase.skipTest("", err)

    An error message is returned instead of skipping the test here so that the test can
    decide if it wants to be skipped or do something else if there is no api key.

    This also sets the global variable API_KEY if it is present

    Args:
        instance (list of strings) part of name of Flywheel instance to look for
    Returns:
        err (str): non-empty if expected instance is in the current API key which means that
            the current user is logged in to the expected instance.
    """

    user_json = Path.home() / ".config/flywheel/user.json"

    if not user_json.exists():
        return f"{str(user_json)} file not found."

    # Check API key is present:
    with open(user_json, "r", encoding="utf8") as f:
        j = json.load(f)
    if "key" not in j or not j["key"]:
        return f"No API key available in {str(user_json)}"
    elif not any([True for ii in instances if ii in j["key"]]):
        return f"Looking for {instances} but API key is for {j['key'].split(':')[0]}"

    global API_KEY  # noqa: PLW0603
    API_KEY = j["key"]
    return "ok"  # (no error) means user is logged in to expected instance
