from unittest.mock import patch

from flywheel_bids.supporting_files.templates import resolve_where_clause


def test_simple_condition_match():
    conditions = {"container_type": "file"}
    context = {"container_type": "file"}
    assert resolve_where_clause(conditions, context) is True


@patch("flywheel_bids.supporting_files.templates.processValueMatch", return_value=False)
def test_simple_condition_no_match(mock_processValueMatch):
    conditions = {"container_type": "file"}
    context = {"container_type": "acquisition"}
    assert resolve_where_clause(conditions, context) is False
    assert mock_processValueMatch.called_once()


def test_regex_condition_match():
    conditions = {"file.name": {"$regex": "events\\.tsv$"}}
    context = {"file": {"name": "sub-01_events.tsv"}}
    assert resolve_where_clause(conditions, context) is True


def test_regex_condition_no_match():
    conditions = {"file.name": {"$regex": "events\\.tsv$"}}
    context = {"file": {"name": "sub-01_events.json"}}
    assert resolve_where_clause(conditions, context) is False


def test_in_condition_match():
    conditions = {
        "file.type": {"$in": ["tabular data", "Tabular Data", "source code", "JSON"]}
    }
    context = {"file": {"type": "tabular data"}}
    assert resolve_where_clause(conditions, context) is True


def test_in_condition_no_match():
    conditions = {
        "file.type": {"$in": ["tabular data", "Tabular Data", "source code", "JSON"]}
    }
    context = {"file": {"type": "image"}}
    assert resolve_where_clause(conditions, context) is False


def test_and_condition_match():
    conditions = {
        "$and": {"container_type": "file", "file.name": {"$regex": "events\\.tsv$"}}
    }
    context = {"container_type": "file", "file": {"name": "sub-01_events.tsv"}}
    assert resolve_where_clause(conditions, context) is True


def test_and_condition_no_match():
    conditions = {
        "$and": {"container_type": "file", "file.name": {"$regex": "events\\.tsv$"}}
    }
    context = {"container_type": "acquisition", "file": {"name": "sub-01_events.tsv"}}
    assert resolve_where_clause(conditions, context) is False


def test_or_condition_match():
    conditions = {
        "$or": [{"container_type": "file"}, {"file.name": {"$regex": "events\\.tsv$"}}]
    }
    context = {"container_type": "acquisition", "file": {"name": "sub-01_events.tsv"}}
    assert resolve_where_clause(conditions, context) is True


def test_or_condition_no_match():
    conditions = {
        "$or": [{"container_type": "file"}, {"file.name": {"$regex": "events\\.tsv$"}}]
    }
    context = {"container_type": "acquisition", "file": {"name": "sub-01_events.json"}}
    assert resolve_where_clause(conditions, context) is False


def test_nested_conditions_match_and():
    conditions = {
        "$and": {
            "file.type": {"$in": ["nifti", "NIfTI"]},
            "$or": [
                {"file.info.ImageType": {"$in": ["ORIGINAL"]}},
                {"file.info.header.dicom.ImageType": {"$in": ["ORIGINAL"]}},
            ],
            "file.classification.Measurement": {"$in": ["Diffusion"]},
            "acquisition.label": {"$regex": ".*(dwi(_.+|-.+|$))"},
        }
    }
    context = {
        "file": {
            "type": "nifti",
            "info": {"ImageType": "ORIGINAL"},
            "classification": {"Measurement": "Diffusion"},
        },
        "acquisition": {"label": "sub-01_dwi"},
    }
    assert resolve_where_clause(conditions, context) is True


def test_nested_conditions_match_or():
    conditions = {
        "$or": [
            {
                "$and": {
                    "file.type": {
                        "$in": ["source code", "JSON", "bvec", "bval", "BVEC", "BVAL"]
                    },
                    "file.classification.Measurement": {"$in": ["Diffusion"]},
                    "file.classification.Intent": {"$in": ["Structural"]},
                }
            },
            {
                "$and": {
                    "file.type": {"$in": ["nifti", "NIfTI"]},
                    "file.info.ImageType": {"$in": ["ORIGINAL"]},
                    "file.classification.Measurement": {"$in": ["Diffusion"]},
                    "acquisition.label": {"$regex": ".*(dwi(_.+|-.+|$))"},
                }
            },
        ]
    }
    context = {
        "file": {
            "type": "nifti",
            "info": {"ImageType": "ORIGINAL"},
            "classification": {"Measurement": "Diffusion"},
        },
        "acquisition": {"label": "sub-01_dwi"},
    }
    assert resolve_where_clause(conditions, context) is True


def test_nested_conditions_complex():
    conditions = {
        "$or": [
            {
                "$and": {
                    "file.type": {"$in": ["nifti", "NIfTI"]},
                    "$or": [
                        {"file.info.ImageType": {"$in": ["ORIGINAL"]}},
                        {"file.info.header.dicom.ImageType": {"$in": ["ORIGINAL"]}},
                    ],
                },
            },
            {
                "$and": {
                    "file.type": {"$in": ["dicom"]},
                    "$or": [
                        {"file.info.ImageType": {"$in": ["DERIVED"]}},
                        {"file.info.header.dicom.ImageType": {"$in": ["DERIVED"]}},
                    ],
                },
            },
        ]
    }
    context = {
        "file": {
            "type": "nifti",
            "info": {"header": {"dicom": {"ImageType": "ORIGINAL"}}},
            "classification": {"Measurement": "Diffusion"},
        },
        "acquisition": {"label": "sub-01_dwi"},
    }
    assert resolve_where_clause(conditions, context) is True


def test_look_ma_no_ands_nested_conditions_complex():
    conditions = {
        "$or": [
            {
                "file.type": {"$in": ["nifti", "NIfTI"]},
                "$or": [
                    {"file.info.ImageType": {"$in": ["ORIGINAL"]}},
                    {"file.info.header.dicom.ImageType": {"$in": ["ORIGINAL"]}},
                ],
            },
            {
                "file.type": {"$in": ["dicom"]},
                "$or": [
                    {"file.info.ImageType": {"$in": ["DERIVED"]}},
                    {"file.info.header.dicom.ImageType": {"$in": ["DERIVED"]}},
                ],
            },
        ]
    }
    context = {
        "file": {
            "type": "nifti",
            "info": {"header": {"dicom": {"ImageType": "ORIGINAL"}}},
            "classification": {"Measurement": "Diffusion"},
        },
        "acquisition": {"label": "sub-01_dwi"},
    }
    assert resolve_where_clause(conditions, context) is True


def test_nested_conditions_complex_no_match():
    conditions = {
        "$or": [
            {
                "$and": {
                    "file.type": {"$in": ["nifti", "NIfTI"]},
                    "$or": [
                        {"file.info.ImageType": {"$in": ["ORIGINAL"]}},
                        {"file.info.header.dicom.ImageType": {"$in": ["ORIGINAL"]}},
                    ],
                },
            },
            {
                "$and": {
                    "file.type": {"$in": ["dicom"]},
                    "$or": [
                        {"file.info.ImageType": {"$in": ["DERIVED"]}},
                        {"file.info.header.dicom.ImageType": {"$in": ["DERIVED"]}},
                    ],
                },
            },
        ]
    }
    # Mix and match the $and criteria
    context = {
        "file": {
            "type": "nifti",
            "info": {"header": {"dicom": {"ImageType": "DERIVED"}}},
            "classification": {"Measurement": "Diffusion"},
        },
        "acquisition": {"label": "sub-01_dwi"},
    }
    assert resolve_where_clause(conditions, context) is False


def test_look_ma_no_ands_nested_conditions_complex_no_match():
    conditions = {
        "$or": [
            {
                "file.type": {"$in": ["nifti", "NIfTI"]},
                "$or": [
                    {"file.info.ImageType": {"$in": ["ORIGINAL"]}},
                    {"file.info.header.dicom.ImageType": {"$in": ["ORIGINAL"]}},
                ],
            },
            {
                "file.type": {"$in": ["dicom"]},
                "$or": [
                    {"file.info.ImageType": {"$in": ["DERIVED"]}},
                    {"file.info.header.dicom.ImageType": {"$in": ["DERIVED"]}},
                ],
            },
        ]
    }
    # Mix and match the $and criteria
    context = {
        "file": {
            "type": "nifti",
            "info": {"header": {"dicom": {"ImageType": "DERIVED"}}},
            "classification": {"Measurement": "Diffusion"},
        },
        "acquisition": {"label": "sub-01_dwi"},
    }
    assert resolve_where_clause(conditions, context) is False
