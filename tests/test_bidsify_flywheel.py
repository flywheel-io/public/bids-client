import os
import shutil
import unittest

from flywheel_bids.supporting_files import bidsify_flywheel, utils
from flywheel_bids.supporting_files.templates import load_template

DEFAULT_TEMPLATE = load_template(None)


class BidsifyTestCases(unittest.TestCase):
    def setUp(self):
        # Define testdir
        self.testdir = "testdir"
        self.maxDiff = None

    def tearDown(self):
        # Cleanup 'testdir', if present
        if os.path.exists(self.testdir):
            shutil.rmtree(self.testdir)

    def test_process_string_template_required(self):
        """ """
        # Define project template from the templates file
        auto_update_str = "sub-<subject.code>_ses-<session.label>_bold.nii.gz"
        # initialize context object
        context = {
            "container_type": "file",
            "parent_container_type": "project",
            "project": {"label": "project123"},
            "subject": {"code": "00123"},
            "session": {"label": "session444"},
            "acquisition": {"label": "acq222"},
            "file": None,
            "ext": None,
        }

        # Call function
        updated_string = utils.process_string_template(auto_update_str, context)

        self.assertEqual(
            updated_string,
            "sub-%s_ses-%s_bold.nii.gz"
            % (
                context["subject"]["code"],
                context["session"]["label"],
            ),
        )

    def test_process_string_template_bids1(self):
        """ """
        # Get project template from the templates file
        auto_update_str = "sub-<subject.code>_ses-<session.label>_bold.nii.gz"
        # initialize context object
        context = {
            "container_type": "file",
            "parent_container_type": "project",
            "project": {"label": "project123"},
            "subject": {"code": "sub-01"},
            "session": {"label": "ses-001"},
            "acquisition": {"label": "acq222"},
            "file": None,
            "ext": None,
        }

        # Call function
        updated_string = utils.process_string_template(auto_update_str, context)

        self.assertEqual(
            updated_string,
            "%s_%s_bold.nii.gz"
            % (context["subject"]["code"], context["session"]["label"]),
        )

    def test_process_string_template_optional(self):
        """ """
        # Define string to auto update, subject code is optional
        auto_update_str = "[sub-<subject.code>]_ses-<session.label>_acq-<acquisition.label>_bold.nii.gz"
        # initialize context object
        context = {
            "container_type": "file",
            "parent_container_type": "project",
            "project": {"label": "project123"},
            "subject": {"code": None},
            "session": {"label": "session444"},
            "acquisition": {"label": "acq222"},
            "file": None,
            "ext": None,
        }

        # Call function
        updated_string = utils.process_string_template(auto_update_str, context)
        # Assert function honors the optional 'sub-<subject.code>'
        self.assertEqual(
            updated_string,
            "_ses-%s_acq-%s_bold.nii.gz"
            % (context["session"]["label"], context["acquisition"]["label"]),
        )

    def test_process_string_template_full_optional(self):
        """ """
        auto_update_str = "sub-<subject.code>[_ses-<session.label>][_acq-{file.info.BIDS.Acq}][_ce-{file.info.BIDS.Ce}][_rec-{file.info.BIDS.Rec}][_run-{file.info.BIDS.Run}][_mod-{file.info.BIDS.Mod}]"
        # initialize context object
        context = {
            "container_type": "file",
            "parent_container_type": "project",
            "project": {"label": "project123"},
            "subject": {"code": "123"},
            "session": {"label": "456"},
            "acquisition": {"label": "acq222"},
            "file": {"classification": {"Measurement": "T1", "Intent": "Structural"}},
            "ext": ".nii.gz",
        }

        # Call function
        updated_string = utils.process_string_template(auto_update_str, context)
        # Assert function honors the optional labels
        self.assertEqual(updated_string, "sub-123_ses-456")

    def test_process_string_template_func_filename1(self):
        """ """
        # Define string to auto update, subject code is optional
        auto_update_str = "sub-<subject.code>[_ses-<session.label>]_task-{file.info.BIDS.Task}_bold{ext}"
        # initialize context object
        context = {
            "container_type": "file",
            "parent_container_type": "project",
            "project": {"label": "project123"},
            "subject": {"code": "001"},
            "session": {"label": "session444"},
            "acquisition": {"label": "acq222"},
            "file": {
                "name": "bold.nii.gz",
                "info": {"BIDS": {"Task": "test123", "Modality": "bold"}},
            },
            "ext": ".nii.gz",
        }

        # Call function
        updated_string = utils.process_string_template(auto_update_str, context)
        # Assert string as expected
        self.assertEqual(
            updated_string,
            "sub-%s_ses-%s_task-%s_%s%s"
            % (
                context["subject"]["code"],
                context["session"]["label"],
                context["file"]["info"]["BIDS"]["Task"],
                context["file"]["info"]["BIDS"]["Modality"],
                context["ext"],
            ),
        )

    def test_process_string_template_required_notpresent(self):
        """ """
        # TODO: Determine the expected behavior of this...
        # Define string to auto update
        auto_update_str = "sub-<subject.code>_ses-<session.label>"
        # initialize context object
        context = {
            "container_type": "file",
            "parent_container_type": "project",
            "project": {"label": "project123"},
            "subject": {},
            "session": {"label": "session444"},
            "acquisition": {"label": "acq222"},
            "file": None,
            "ext": None,
        }

        # Call function
        updated_string = utils.process_string_template(auto_update_str, context)
        # Assert function honors the optional 'sub-<subject.code>'
        self.assertEqual(
            updated_string, "sub-<subject.code>_ses-%s" % (context["session"]["label"])
        )

    def test_process_string_template_required_None(self):
        """ """
        # TODO: Determine the expected behavior of this...
        # Define string to auto update
        auto_update_str = "sub-<subject.code>_ses-<session.label>"
        # initialize context object
        context = {
            "container_type": "file",
            "parent_container_type": "project",
            "project": {"label": "project123"},
            "subject": {"code": None},
            "session": {"label": "session444"},
            "acquisition": {"label": "acq222"},
            "file": None,
            "ext": None,
        }

        # Call function
        updated_string = utils.process_string_template(auto_update_str, context)
        # Assert function honors the optional 'sub-<subject.code>'
        self.assertEqual(
            updated_string, "sub-<subject.code>_ses-%s" % (context["session"]["label"])
        )

    def test_add_properties_valid(self):
        """ """
        properties = {
            "Filename": {
                "type": "string",
                "label": "Filename",
                "default": "",
                "auto_update": "sub-<subject.code>_ses-<session.label>[_acq-<acquisition.label>]_T1w{ext}",
            },
            "Folder": {"type": "string", "label": "Folder", "default": "anat"},
            "Ce": {"type": "string", "label": "CE Label", "default": ""},
            "Rec": {"type": "string", "label": "Rec Label", "default": ""},
            "Run": {"type": "string", "label": "Run Index", "default": ""},
            "Mod": {"type": "string", "label": "Mod Label", "default": ""},
            "Modality": {
                "type": "string",
                "label": "Modality Label",
                "default": "T1w",
                "enum": [
                    "T1w",
                    "T2w",
                    "T1rho",
                    "T1map",
                    "T2map",
                    "FLAIR",
                    "FLASH",
                    "PD",
                    "PDmap",
                    "PDT2",
                    "inplaneT1",
                    "inplaneT2",
                    "angio",
                    "defacemask",
                    "SWImagandphase",
                ],
            },
        }
        project_obj = {"label": "Project Name"}
        # Call function
        info_obj = bidsify_flywheel.add_properties(
            properties, project_obj, ["anatomy_t1w"]
        )
        # Expected info object
        for key in properties:
            project_obj[key] = properties[key]["default"]
        self.assertEqual(info_obj, project_obj)

    def test_update_properties_valid(self):
        """ """
        # Define inputs
        properties = {
            "Filename": {
                "type": "string",
                "label": "Filename",
                "default": "",
                "auto_update": "sub-<subject.code>_ses-<session.label>[_acq-<acquisition.label>]_T1w{ext}",
            },
            "Folder": {"type": "string", "label": "Folder", "default": "anat"},
            "Mod": {"type": "string", "label": "Mod Label", "default": ""},
            "Modality": {"type": "string", "label": "Modality Label", "default": "T1w"},
        }
        context = {
            "container_type": "file",
            "parent_container_type": "acquisition",
            "project": None,
            "subject": {"code": "001"},
            "session": {"label": "sesTEST"},
            "acquisition": {"label": "acqTEST"},
            "file": {
                "classification": {"Measurement": "T1", "Intent": "Structural"},
                "type": "nifti",
            },
            "ext": ".nii.gz",
        }
        project_obj = {"test1": "123", "test2": "456"}
        # Call function
        info_obj = bidsify_flywheel.update_properties(properties, context, project_obj)
        # Update project_obj, as expected
        project_obj["Filename"] = "sub-%s_ses-%s_acq-%s_T1w%s" % (
            context["subject"]["code"],
            context["session"]["label"],
            context["acquisition"]["label"],
            context["ext"],
        )
        self.assertEqual(project_obj, info_obj)

    def test_process_matching_templates_anat_t1w(self):
        """ """
        # Define context
        context = {
            "container_type": "file",
            "parent_container_type": "acquisition",
            "project": None,
            "subject": {"code": "001"},
            "session": {
                "label": "sesTEST",
                "info": {"BIDS": {"Label": "sesTEST", "Subject": "001"}},
            },
            "acquisition": {"label": "acqTEST"},
            "file": {
                "classification": {"Measurement": ["T1"], "Intent": "Structural"},
                "type": "nifti",
            },
            "ext": ".nii.gz",
        }

        # Call function
        container = bidsify_flywheel.process_matching_templates(
            context, DEFAULT_TEMPLATE
        )
        # Define expected container
        container_expected = {
            "info": {
                "BIDS": {
                    "template": "anat_file",
                    "rule_id": "bids_anat_file",
                    "Filename": "sub-001_ses-sesTEST_T1w.nii.gz",
                    "Path": "sub-001/ses-sesTEST/anat",
                    "Folder": "anat",
                    "Run": "",
                    "Acq": "",
                    "Ce": "",
                    "Rec": "",
                    "Suffix": "T1w",
                    "Mod": "",
                    "ignore": False,
                }
            },
            "classification": {"Measurement": ["T1"], "Intent": "Structural"},
            "type": "nifti",
        }
        self.assertEqual(container, container_expected)

    def test_process_matching_templates_anat_t2w(self):
        """ """
        # Define context
        context = {
            "container_type": "file",
            "parent_container_type": "acquisition",
            "project": None,
            "subject": {"code": "001"},
            "session": {
                "label": "sesTEST",
                "info": {"BIDS": {"Label": "sesTEST", "Subject": "001"}},
            },
            "acquisition": {"label": "acqTEST"},
            "file": {
                "classification": {"Measurement": ["T2"], "Intent": "Structural"},
                "type": "nifti",
            },
            "ext": ".nii.gz",
        }
        # Call function
        container = bidsify_flywheel.process_matching_templates(
            context, DEFAULT_TEMPLATE
        )
        # Define expected container
        container_expected = {
            "info": {
                "BIDS": {
                    "template": "anat_file",
                    "rule_id": "bids_anat_file",
                    "Filename": "sub-001_ses-sesTEST_T2w.nii.gz",
                    "Path": "sub-001/ses-sesTEST/anat",
                    "Folder": "anat",
                    "Run": "",
                    "Acq": "",
                    "Ce": "",
                    "Rec": "",
                    "Suffix": "T2w",
                    "Mod": "",
                    "ignore": False,
                }
            },
            "classification": {"Measurement": ["T2"], "Intent": "Structural"},
            "type": "nifti",
        }
        self.assertEqual(container, container_expected)

    def test_process_matching_templates_func(self):
        """ """
        # Define context
        context = {
            "container_type": "file",
            "parent_container_type": "acquisition",
            "project": None,
            "subject": {"code": "001"},
            "session": {
                "label": "sesTEST",
                "info": {"BIDS": {"Label": "sesTEST", "Subject": "001"}},
            },
            "run_counters": utils.RunCounterMap(),
            "acquisition": {"label": "acq_task-TEST_run+"},
            "file": {
                "classification": {"Intent": "Functional"},
                "type": "nifti",
            },
            "ext": ".nii.gz",
        }
        # Call function
        container = bidsify_flywheel.process_matching_templates(
            context, DEFAULT_TEMPLATE
        )
        # Define expected container
        container_expected = {
            "classification": {"Intent": "Functional"},
            "type": "nifti",
            "info": {
                "BIDS": {
                    "template": "func_file",
                    "rule_id": "bids_func_file",
                    "Filename": "sub-001_ses-sesTEST_task-TEST_bold.nii.gz",
                    "Folder": "func",
                    "Path": "sub-001/ses-sesTEST/func",
                    "Acq": "",
                    "Ce": "",
                    "Dir": "",
                    "Echo": "",
                    "Rec": "",
                    "Run": "",
                    "Suffix": "bold",
                    "Task": "TEST",
                    "sidecarChanges": {"TaskName": "TEST"},
                    "ignore": False,
                }
            },
        }
        self.assertEqual(container, container_expected)

    def test_process_matching_templates_task_events(self):
        """"""
        # Define context
        context = {
            "container_type": "file",
            "parent_container_type": "acquisition",
            "project": None,
            "subject": {"code": "001"},
            "session": {
                "label": "sesTEST",
                "info": {"BIDS": {"Label": "sesTEST", "Subject": "001"}},
            },
            "acquisition": {"label": "acqTEST"},
            "file": {
                "classification": {"Intent": "Functional"},
                "type": "tabular data",
                "name": "sesTEST.tsv",
            },
            "ext": ".tsv",
        }
        # Call function
        container = bidsify_flywheel.process_matching_templates(
            context, DEFAULT_TEMPLATE
        )
        # Define expected container
        container_expected = {
            "classification": {"Intent": "Functional"},
            "type": "tabular data",
            "name": "sesTEST.tsv",
            "info": {
                "BIDS": {
                    "template": "func_events_file",
                    "rule_id": "bids_func_events_file",
                    "Filename": "sub-001_ses-sesTEST_task-{file.info.BIDS.Task}_events.tsv",
                    "Folder": "func",
                    "Path": "sub-001/ses-sesTEST/func",
                    "Acq": "",
                    "Ce": "",
                    "Dir": "",
                    "Echo": "",
                    "Rec": "",
                    "Run": "",
                    "Task": "",
                    "ignore": False,
                }
            },
        }
        self.assertEqual(container, container_expected)

    def test_process_matching_beh_events_file(self):
        """"""
        # Define context
        context = {
            "container_type": "file",
            "parent_container_type": "acquisition",
            "project": None,
            "subject": {"code": "001"},
            "session": {
                "label": "sesTEST",
                "info": {"BIDS": {"Label": "sesTEST", "Subject": "001"}},
            },
            "acquisition": {"label": "acqTEST", "id": "acqID"},
            "file": {
                "name": "sesTEST.tsv",
                "classification": {"Custom": "Behavioral"},
                "type": "tabular data",
            },
            "ext": ".tsv",
        }
        # Call function
        container = bidsify_flywheel.process_matching_templates(
            context, DEFAULT_TEMPLATE
        )
        # Define expected container
        container_expected = {
            "name": "sesTEST.tsv",
            "classification": {"Custom": "Behavioral"},
            "type": "tabular data",
            "info": {
                "BIDS": {
                    "template": "beh_events_file",
                    "rule_id": "bids_beh_events_file",
                    "Filename": "sub-001_ses-sesTEST_task-{file.info.BIDS.Task}_events.tsv",
                    "Folder": "beh",
                    "Path": "sub-001/ses-sesTEST/beh",
                    "Acq": "",
                    "Run": "",
                    "Suffix": "events",
                    "Task": "",
                    "ignore": False,
                }
            },
        }
        self.assertEqual(container, container_expected)

    def test_process_matching_templates_physio_task_events(self):
        """"""
        # Define context
        context = {
            "container_type": "file",
            "parent_container_type": "acquisition",
            "project": None,
            "subject": {"code": "001"},
            "session": {
                "label": "sesTEST",
                "info": {"BIDS": {"Label": "sesTEST", "Subject": "001"}},
            },
            "acquisition": {"label": "acqTEST", "id": "acqID"},
            "file": {
                "classification": {"Custom": "Physio"},
                "type": "tabular data",
                "name": "sesTEST.tsv",
            },
            "ext": ".tsv",
        }
        # Call function
        container = bidsify_flywheel.process_matching_templates(
            context, DEFAULT_TEMPLATE
        )
        # Define expected container
        container_expected = {
            "classification": {"Custom": "Physio"},
            "type": "tabular data",
            "name": "sesTEST.tsv",
        }
        self.assertEqual(container, container_expected)

    def test_process_matching_templates_dwi_nifti(self):
        """ """
        # Define context
        context = {
            "container_type": "file",
            "parent_container_type": "acquisition",
            "project": None,
            "subject": {"code": "001"},
            "session": {
                "label": "sesTEST",
                "info": {"BIDS": {"Label": "sesTEST", "Subject": "001"}},
            },
            "acquisition": {"label": "acqTEST"},
            "file": {
                "classification": {
                    "Measurement": "Diffusion",
                    "Intent": "Structural",
                },
                "type": "nifti",
            },
            "ext": ".nii.gz",
        }
        # Call function
        container = bidsify_flywheel.process_matching_templates(
            context, DEFAULT_TEMPLATE
        )
        # Define expected container
        container_expected = {
            "info": {
                "BIDS": {
                    "template": "dwi_file",
                    "rule_id": "bids_dwi_file",
                    "Filename": "sub-001_ses-sesTEST_dwi.nii.gz",
                    "Path": "sub-001/ses-sesTEST/dwi",
                    "Folder": "dwi",
                    "Suffix": "dwi",
                    "Acq": "",
                    "Run": "",
                    "Dir": "",
                    "ignore": False,
                }
            },
            "classification": {"Measurement": "Diffusion", "Intent": "Structural"},
            "type": "nifti",
        }
        self.assertEqual(container, container_expected)

    def test_process_matching_templates_dwi_bval(self):
        """ """
        # Define context
        context = {
            "container_type": "file",
            "parent_container_type": "acquisition",
            "project": None,
            "subject": {"code": "001"},
            "session": {
                "label": "sesTEST",
                "info": {"BIDS": {"Label": "sesTEST", "Subject": "001"}},
            },
            "acquisition": {"label": "acqTEST"},
            "file": {
                "classification": {
                    "Measurement": "Diffusion",
                    "Intent": "Structural",
                },
                "type": "bval",
            },
            "ext": ".bval",
        }
        # Call function
        container = bidsify_flywheel.process_matching_templates(
            context, DEFAULT_TEMPLATE
        )
        # Define expected container
        container_expected = {
            "info": {
                "BIDS": {
                    "template": "dwi_file",
                    "rule_id": "bids_dwi_file",
                    "Filename": "sub-001_ses-sesTEST_dwi.bval",
                    "Path": "sub-001/ses-sesTEST/dwi",
                    "Folder": "dwi",
                    "Suffix": "dwi",
                    "Acq": "",
                    "Run": "",
                    "Dir": "",
                    "ignore": False,
                }
            },
            "classification": {"Measurement": "Diffusion", "Intent": "Structural"},
            "type": "bval",
        }
        self.assertEqual(container, container_expected)

    def test_process_matching_templates_dwi_bvec(self):
        """ """
        # Define context
        context = {
            "container_type": "file",
            "parent_container_type": "acquisition",
            "project": None,
            "subject": {"code": "001"},
            "session": {
                "label": "sesTEST",
                "info": {"BIDS": {"Label": "sesTEST", "Subject": "001"}},
            },
            "acquisition": {"label": "acqTEST"},
            "file": {
                "classification": {
                    "Measurement": "Diffusion",
                    "Intent": "Structural",
                },
                "type": "bvec",
            },
            "ext": ".bvec",
        }
        # Call function
        container = bidsify_flywheel.process_matching_templates(
            context, DEFAULT_TEMPLATE
        )
        # Define expected container
        container_expected = {
            "info": {
                "BIDS": {
                    "template": "dwi_file",
                    "rule_id": "bids_dwi_file",
                    "Filename": "sub-001_ses-sesTEST_dwi.bvec",
                    "Path": "sub-001/ses-sesTEST/dwi",
                    "Folder": "dwi",
                    "Suffix": "dwi",
                    "Acq": "",
                    "Run": "",
                    "Dir": "",
                    "ignore": False,
                }
            },
            "classification": {"Measurement": "Diffusion", "Intent": "Structural"},
            "type": "bvec",
        }
        self.assertEqual(container, container_expected)

    def test_process_matching_templates_fieldmap(self):
        """"""
        # Define context
        context = {
            "container_type": "file",
            "parent_container_type": "acquisition",
            "project": None,
            "subject": {"code": "001"},
            "session": {
                "label": "sesTEST",
                "info": {"BIDS": {"Label": "sesTEST", "Subject": "001"}},
            },
            "acquisition": {"label": "acqTEST"},
            "file": {
                "classification": {"Intent": "Fieldmap"},
                "type": "nifti",
            },
            "ext": ".nii.gz",
        }
        # Call function
        container = bidsify_flywheel.process_matching_templates(
            context, DEFAULT_TEMPLATE
        )
        # Define expected container
        container_expected = {
            "info": {
                "BIDS": {
                    "template": "fmap_file",
                    "rule_id": "bids_fmap_file",
                    "Filename": "sub-001_ses-sesTEST_fieldmap.nii.gz",
                    "Folder": "fmap",
                    "Path": "sub-001/ses-sesTEST/fmap",
                    "Acq": "",
                    "Run": "",
                    "Suffix": "fieldmap",
                    "IntendedFor": [
                        {"Folder": "anat"},
                        {"Folder": "func"},
                        {"Folder": "dwi"},
                    ],
                    "ignore": False,
                }
            },
            "classification": {"Intent": "Fieldmap"},
            "type": "nifti",
        }
        self.assertEqual(container, container_expected)

    def test_process_matching_templates_fieldmap_phase_encoded(self):
        """"""
        # Define context
        context = {
            "container_type": "file",
            "parent_container_type": "acquisition",
            "project": None,
            "subject": {"code": "001"},
            "session": {
                "label": "sesTEST",
                "info": {"BIDS": {"Label": "sesTEST", "Subject": "001"}},
            },
            "acquisition": {
                "label": "acqTEST Topup PA"
            },  # Acquisition label needs to contain
            "file": {"classification": {"Intent": "Fieldmap"}, "type": "nifti"},
            "ext": ".nii.gz",
        }
        # Call function
        container = bidsify_flywheel.process_matching_templates(
            context, DEFAULT_TEMPLATE
        )
        # Define expected container
        container_expected = {
            "classification": {"Intent": "Fieldmap"},
            "type": "nifti",
            "info": {
                "BIDS": {
                    "template": "fmap_file",
                    "rule_id": "bids_fmap_file",
                    "Filename": "sub-001_ses-sesTEST_fieldmap.nii.gz",
                    "Folder": "fmap",
                    "Path": "sub-001/ses-sesTEST/fmap",
                    "Acq": "",
                    "Run": "",
                    "Suffix": "fieldmap",
                    "IntendedFor": [
                        {"Folder": "anat"},
                        {"Folder": "func"},
                        {"Folder": "dwi"},
                    ],
                    "ignore": False,
                }
            },
        }
        self.assertEqual(container, container_expected)

    def test_process_matching_templates_dicom(self):
        # Define context
        context = {
            "container_type": "file",
            "parent_container_type": "acquisition",
            "project": {"label": "hello"},
            "subject": {"code": "001"},
            "session": {
                "label": "sesTEST",
                "info": {"BIDS": {"Label": "sesTEST", "Subject": "001"}},
            },
            "acquisition": {"label": "acqTEST"},
            "file": {
                "classification": {
                    "Measurement": "Diffusion",
                    "Intent": "Structural",
                },
                "type": "dicom",
            },
            "ext": ".dcm.zip",
        }
        # Call function
        container = bidsify_flywheel.process_matching_templates(
            context, DEFAULT_TEMPLATE
        )
        # Define expected container
        container_expected = {
            "info": {
                "BIDS": {
                    "template": "dicom_file",
                    "rule_id": "bids_dicom_file",
                    "Filename": "",
                    "Folder": "sourcedata",
                    "Path": "sourcedata/sub-001/ses-sesTEST",
                    "ignore": False,
                }
            },
            "classification": {"Measurement": "Diffusion", "Intent": "Structural"},
            "type": "dicom",
        }
        self.assertEqual(container, container_expected)

    def test_process_matching_templates_non_bids_dicom(self):
        # Define context
        context = {
            "container_type": "file",
            "parent_container_type": "acquisition",
            "project": {"label": "hello"},
            "subject": {"code": "001"},
            "session": {
                "label": "sesTEST",
                "info": {"BIDS": {"Label": "sesTEST", "Subject": "001"}},
            },
            "acquisition": {"label": "acqTEST", "id": "09090"},
            "file": {
                "name": "4784_1_1_localizer",
                "classification": {"Measurement": "T2", "Intent": "Localizer"},
                "type": "dicom",
            },
            "ext": ".dcm.zip",
        }
        # Call function
        container = bidsify_flywheel.process_matching_templates(
            context, DEFAULT_TEMPLATE
        )
        # Define expected container
        container_expected = {
            "name": "4784_1_1_localizer",
            "classification": {"Measurement": "T2", "Intent": "Localizer"},
            "type": "dicom",
        }
        self.assertEqual(container, container_expected)

    def test_resolve_initial_dicom_field_values_from_filename(self):
        # Define context
        context = {
            "container_type": "file",
            "parent_container_type": "acquisition",
            "project": {"label": "hello"},
            "subject": {"code": "001"},
            "session": {
                "label": "sesTEST",
                "info": {"BIDS": {"Label": "sesTEST", "Subject": "001"}},
            },
            "acquisition": {"label": "acqTEST"},
            "file": {
                "name": "09 cmrr_mbepi_task-spatialfrequency_s6_2mm_66sl_PA_TR1.0.dcm.zip",
                "classification": {
                    "Measurement": "Diffusion",
                    "Intent": "Structural",
                },
                "type": "dicom",
            },
            "ext": ".dcm.zip",
        }
        # Call function
        container = bidsify_flywheel.process_matching_templates(
            context, DEFAULT_TEMPLATE
        )
        # Define expected container
        container_expected = {
            "info": {
                "BIDS": {
                    "template": "dicom_file",
                    "rule_id": "bids_dicom_file",
                    "Filename": "09 cmrr_mbepi_task-spatialfrequency_s6_2mm_66sl_PA_TR1.0.dcm.zip",
                    "Folder": "sourcedata",
                    "Path": "sourcedata/sub-001/ses-sesTEST",
                    "ignore": False,
                }
            },
            "name": "09 cmrr_mbepi_task-spatialfrequency_s6_2mm_66sl_PA_TR1.0.dcm.zip",
            "classification": {"Measurement": "Diffusion", "Intent": "Structural"},
            "type": "dicom",
        }
        self.assertEqual(container, container_expected)

    def test_process_matching_template_acquisition(self):
        """ """
        # Define context
        context = {
            "container_type": "acquisition",
            "parent_container_type": "session",
            "project": {"label": "Project_Label_Test"},
            "subject": None,
            "session": {"label": "Session_Label_Test"},
            "acquisition": {"label": "Acquisition_Label_Test"},
            "file": {},
            "ext": ".zip",
        }
        # Call function
        container = bidsify_flywheel.process_matching_templates(
            context, DEFAULT_TEMPLATE
        )
        # Define expected container
        container_expected = {
            "info": {
                "BIDS": {
                    "template": "acquisition",
                    "rule_id": "bids_acquisition",
                    "ignore": False,
                }
            },
            "label": "Acquisition_Label_Test",
        }
        self.assertEqual(container, container_expected)

    def test_process_matching_templates_acquisition_file(self):
        """ """
        # Define context
        context = {
            "container_type": "file",
            "parent_container_type": "acquisition",
            "project": {"label": "testproject"},
            "subject": {"code": "12345"},
            "session": {
                "label": "haha",
                "info": {"BIDS": {"Label": "haha", "Subject": "12345"}},
            },
            "acquisition": {"label": "blue", "id": "ID"},
            "file": {"type": "image", "name": "fname"},
            "ext": ".jpg",
        }
        # Won't match if not on upload
        container = bidsify_flywheel.process_matching_templates(
            context, DEFAULT_TEMPLATE
        )
        # Define expected container
        container_expected = {"type": "image", "name": "fname"}
        self.assertEqual(container, container_expected)
        # Call function
        container = bidsify_flywheel.process_matching_templates(
            context, DEFAULT_TEMPLATE, upload=True
        )
        # Define expected container
        container_expected = {
            "info": {
                "BIDS": {
                    "template": "acquisition_file",
                    "rule_id": "bids_acquisition_file",
                    "Filename": "",
                    "Folder": "acq-blue",
                    "Path": "sub-12345/ses-haha/acq-blue",
                    "ignore": False,
                }
            },
            "type": "image",
            "name": "fname",
        }
        self.assertEqual(container, container_expected)

    def test_process_matching_templates_session(self):
        """ """
        # Define context
        context = {
            "container_type": "session",
            "parent_container_type": "project",
            "project": {"label": "Project_Label_Test"},
            "subject": {"code": "12345"},
            "session": {"label": "Session_Label_Test"},
            "acquisition": None,
            "file": {},
            "ext": ".zip",
        }
        # Call function
        container = bidsify_flywheel.process_matching_templates(
            context, DEFAULT_TEMPLATE
        )
        # Define expected container
        container_expected = {
            "info": {
                "BIDS": {
                    "Label": "SessionLabelTest",
                    "Subject": "12345",
                    "template": "session",
                    "rule_id": "bids_session",
                    "ignore": False,
                }
            },
            "label": "Session_Label_Test",
        }
        self.assertEqual(container, container_expected)

    def test_process_matching_templates_session_file(self):
        """ """
        # Define context
        context = {
            "container_type": "file",
            "parent_container_type": "session",
            "project": {"label": "testproject"},
            "subject": {"code": "12345"},
            "session": {
                "label": "sesTEST",
                "info": {"BIDS": {"Label": "sesTEST", "Subject": "12345"}},
            },
            "acquisition": None,
            "file": {"type": "tabular"},
            "ext": ".tsv",
        }
        # Call function
        container = bidsify_flywheel.process_matching_templates(
            context, DEFAULT_TEMPLATE
        )
        # Define expected container
        container_expected = {
            "info": {
                "BIDS": {
                    "template": "session_file",
                    "rule_id": "bids_session_file",
                    "Filename": "",
                    "Folder": "ses-sesTEST",
                    "Path": "sub-12345/ses-sesTEST",
                    "ignore": False,
                }
            },
            "type": "tabular",
        }
        self.assertEqual(container, container_expected)

    def test_process_matching_templates_project(self):
        """ """
        # Define context
        context = {
            "container_type": "project",
            "parent_container_type": "group",
            "project": {"label": "Project_Label_Test"},
            "subject": None,
            "session": None,
            "acquisition": None,
            "file": {},
            "ext": ".zip",
        }
        # Call function
        container = bidsify_flywheel.process_matching_templates(
            context, DEFAULT_TEMPLATE
        )
        # Define expected container
        container_expected = {
            "label": "Project_Label_Test",
            "info": {
                "BIDS": {
                    "Sidecar": "data is in json sidecar, not file.info",
                    "rule_id": "bids_project",
                    "template": "project",
                }
            },
        }
        self.assertEqual(container, container_expected)

    def test_process_matching_templates_project_file(self):
        """ """
        # Define context
        context = {
            "container_type": "file",
            "parent_container_type": "project",
            "project": None,
            "subject": None,
            "session": None,
            "acquisition": None,
            "file": {"classification": {}, "type": "archive"},
            "ext": ".zip",
        }
        # Call function
        container = bidsify_flywheel.process_matching_templates(
            context, DEFAULT_TEMPLATE
        )
        # Define expected container
        container_expected = {
            "info": {
                "BIDS": {
                    "template": "project_file",
                    "rule_id": "bids_project_file",
                    "Filename": "",
                    "Folder": "",
                    "Path": "",
                    "ignore": False,
                }
            },
            "classification": {},
            "type": "archive",
        }
        self.assertEqual(container, container_expected)

    def test_process_matching_templates_BIDS_NA(self):
        """ """
        # Define context
        context = {
            "container_type": "file",
            "parent_container_type": "acquisition",
            "project": None,
            "subject": {"code": "001"},
            "session": {
                "label": "sesTEST",
                "info": {"BIDS": {"Label": "sesTEST", "Subject": "001"}},
            },
            "run_counters": utils.RunCounterMap(),
            "acquisition": {"label": "acq_task-TEST_run+"},
            "file": {
                "classification": {"Intent": "Functional"},
                "type": "nifti",
                "info": {"BIDS": "NA"},
            },
            "ext": ".nii.gz",
        }
        # Call function
        container = bidsify_flywheel.process_matching_templates(
            context, DEFAULT_TEMPLATE
        )
        # Define expected container
        container_expected = {
            "classification": {"Intent": "Functional"},
            "type": "nifti",
            "info": {"BIDS": "NA"},
        }

        self.assertEqual(container, container_expected)

    def assertEqual(self, a, b):
        a = utils.normalize_strings(a)
        b = utils.normalize_strings(b)

        unittest.TestCase.assertEqual(self, a, b)


if __name__ == "__main__":
    unittest.main()
