import pytest

from flywheel_bids.supporting_files.classifications import (
    determine_modality,
    search_classifications,
)


@pytest.mark.parametrize(
    "test_name, expected_mod",
    [("PCASL", "perf"), ("A_T1W_image", "anat"), ("fieldmap_for_a_fieldmouse", "fmap")],
)
def test_determine_modality_succeeds(test_name, expected_mod):
    result = determine_modality(test_name)
    assert result == expected_mod


def test_determine_modality_logs_error(caplog):
    result = determine_modality("a_random_non_bids_image_or_file")
    assert "Could not match modality" in caplog.records[0].message
    assert not result


@pytest.mark.parametrize(
    "test_dir, test_name, expected",
    [
        ("func", "an_fMRI_bold_image", {"Intent": "Functional"}),
        (
            "fmap",
            "not_an_asl_fieldmap_magnitude1",
            {"Measurement": "B0", "Intent": "Fieldmap"},
        ),
        (
            "perf",
            "searching_for_an-aslcontext",
            {"Measurement": "Perfusion", "Custom": "Context"},
        ),
    ],
)
def test_search_classifications_succeeds(test_dir, test_name, expected):
    result = search_classifications(test_dir, test_name)
    assert result == expected


@pytest.mark.parametrize(
    "test_dir, test_name",
    [("func", "fieldmap_magnitude1_should_be_fmap"), ("perf", "misspelled_pcslcntxt")],
)
def test_search_classificiations_logs_error(test_dir, test_name, caplog):
    result = search_classifications(test_dir, test_name)
    assert not result
    assert (
        f"Did not find info in the classification table under {test_dir} for {test_name}"
        in caplog.records[0].message
    )
