import logging
from pathlib import PosixPath
from unittest.mock import MagicMock

import pytest
from flywheel import Project

from flywheel_bids.utils.curation_tools import find_how_curated


@pytest.fixture(autouse=True)
def setup_logging(caplog):
    caplog.set_level(logging.INFO)
    caplog.clear()


@pytest.fixture
def mock_project():
    return MagicMock(spec=Project)


class CustomPath(PosixPath):
    @property
    def _name(self):
        return self.as_posix()

    def __getattr__(self, name):
        if name == "name":
            return self._name
        raise AttributeError(
            f"'{type(self).__name__}' object has no attribute '{name}'"
        )


@pytest.mark.parametrize(
    "project_config, namespace, save_sidecar_as_metadata, expected_ignore_result, expected_log",
    [
        # New way - force using sidecars, not metadata; project not curated yet
        (
            {},
            "BIDS",
            "no",
            False,
            ["How Curated: an argument was provided to use the data in sidecars"],
        ),
        # Old way - force using metadata, not sidecars; project not curated yet
        (
            {},
            "BIDS",
            "yes",
            True,
            ["How Curated: an argument was provided to store sidecar data in NIfTI"],
        ),
        # Old way - BIDS metadata on project
        (
            {"BIDS": {"Acknowledgements": "Test"}},
            "BIDS",
            "auto",
            True,
            [
                "dataset_description.json information has been found in BIDS project metadata"
            ],
        ),
        # New way - Sidecar in BIDS metadata
        (
            {"BIDS": {"Sidecar": "data is in json sidecar, not file.info"}},
            "BIDS",
            "auto",
            False,
            ["'Sidecar' has been found in BIDS project metadata so json sidecar"],
        ),
        # New way - force using sidecars, not metadata; project curated the old way
        (
            {"BIDS": {"Acknowledgements": "Test"}},
            "BIDS",
            "no",
            False,
            [
                "an argument was provided to use the data in sidecars",
                "WARNING",
                "The project has been curated the old way, but the user has requested to use sidecar files",
            ],
        ),
        # Old way - force sidecar data in metadata; project curated the new way
        (
            {"BIDS": {"Sidecar": "data is in json sidecar, not file.info"}},
            "BIDS",
            "yes",
            True,
            [
                "an argument was provided to store sidecar data in NIfTI",
                "WARNING",
                "The project has been curated the new way, but the user has requested to ignore sidecar files",
            ],
        ),
        # New way - Project not curated, no request to use sidecars or not
        (
            {},
            "BIDS",
            "",
            False,
            [
                "no information is available to determine if sidecar data is stored in sidecar files or in the NIfTI"
            ],
        ),
    ],
)
def test_find_how_curated(
    mock_project,
    project_config,
    namespace,
    save_sidecar_as_metadata,
    expected_ignore_result,
    expected_log,
    caplog,
):
    # Configure the mock project
    # Test cases for successful execution
    result = find_how_curated(project_config, namespace, save_sidecar_as_metadata)
    assert result == expected_ignore_result
    for expected_text in expected_log:
        assert expected_text in caplog.text
