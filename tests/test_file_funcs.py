from pathlib import Path
from unittest.mock import MagicMock, PropertyMock, patch

import flywheel
import pytest
from flywheel_gear_toolkit.testing.hierarchy import MockFileEntry, make_acquisition
from flywheel_gear_toolkit.utils.walker import Walker

from flywheel_bids.supporting_files.file_funcs import (
    copy_matching_sbrefs,
    copy_single_sbref,
    create_sbref_name,
    list_func_scans,
    list_sbref_scans,
)


@pytest.fixture
def mock_func_acq():
    mock_acq = make_acquisition("func_mock")
    return mock_acq


success_list = [
    MockFileEntry(
        parent=mock_func_acq,
        name="ASL.nii",
        type="NIfTI",
        file_id=1,
        classification={"Intent": "Functional", "Measurement": "feet", "Features": []},
    ),
    MockFileEntry(
        parent=mock_func_acq,
        name="T1.nii.gz",
        type="NIfTI",
        file_id=2,
        classification={"Intent": "Structural", "Measurement": "worms"},
    ),
    MockFileEntry(
        parent=mock_func_acq,
        name="rs-fMRI.nii",
        type="NIfTI",
        file_id=3,
        classification={
            "Intent": "Functional",
            "Measurement": "T2*",
            "Features": ["Task"],
        },
    ),
    MockFileEntry(
        parent=mock_func_acq,
        name="rs-fMRI2.nii",
        type="NIfTI",
        file_id=4,
        classification={
            "Intent": "Functional",
            "Measurement": "T2*",
            "Features": ["Task"],
        },
    ),
    MockFileEntry(
        parent=mock_func_acq,
        name="the_sbref.nii.gz",
        type="NIfTI",
        file_id=5,
        classification={
            "Intent": "Functional",
            "Measurement": "T2*",
            "Features": ["SBRef"],
        },
    ),
]
failure_list = [
    MockFileEntry(
        parent=mock_func_acq,
        name="rs-fMRI.nii",
        type="NIfTI",
        file_id=2,
        classification={
            "Intent": "Functional",
            "Measurement": "T2*",
            "Features": ["Task"],
        },
    ),
    MockFileEntry(
        parent=mock_func_acq,
        name="the_failure_sbref.nii.gz",
        type="NIfTI",
        file_id=5,
        classification={
            "Intent": "Functional",
            "Measurement": "T2*",
            "Features": ["SBRef"],
        },
    ),
]


@pytest.mark.parametrize(
    "func_ix_start, func_ix_end, sbref_ix_start, sbref_ix_end, expected_copy_calls",
    [(2, 4, 4, 5, 2), (2, 4, 3, 5, 0)],
)
@patch("flywheel_bids.supporting_files.file_funcs.op.join")
@patch("flywheel_bids.supporting_files.file_funcs.create_sbref_name")
@patch("flywheel_bids.supporting_files.file_funcs.copy_single_sbref")
@patch("flywheel_bids.supporting_files.file_funcs.list_sbref_scans")
@patch("flywheel_bids.supporting_files.file_funcs.list_func_scans")
def test_copy_matching_sbrefs(
    mock_funcs,
    mock_sbrefs,
    mock_copy,
    mock_name,
    mock_join,
    func_ix_start,
    func_ix_end,
    sbref_ix_start,
    sbref_ix_end,
    expected_copy_calls,
    mock_context,
):
    mock_funcs.return_value = success_list[func_ix_start:func_ix_end]
    mock_sbrefs.return_value = success_list[sbref_ix_start:sbref_ix_end]
    copy_matching_sbrefs(mock_context)
    assert mock_copy.call_count == expected_copy_calls


def test_copy_single_sbref(mock_func_acq):
    mock_sbref_acqs = MagicMock()
    mock_func_acq.parent = PropertyMock()
    mock_func_acq.upload_file = MagicMock()
    mock_sb_name = "here/I/am.nii"

    copy_single_sbref(mock_sbref_acqs, mock_sb_name, mock_func_acq)
    assert mock_sbref_acqs.download.called
    # assert mock_func_acq.upload_file.called


@pytest.mark.parametrize(
    "mock_sbref, expected_name",
    [
        ("scooby-doo.tar.gz", "matilda_sbref.tar.gz"),
        ("mickey.nii", "matilda_sbref.nii"),
    ],
)
def test_create_sbref_name(mock_sbref, expected_name):
    mock_func_scan = "matilda.nii.gz"
    output = create_sbref_name(mock_sbref, mock_func_scan)
    assert output == expected_name


@pytest.mark.parametrize(
    "mock_acq_list, expected_list",
    [(success_list, ["rs-fMRI.nii", "rs-fMRI2.nii"]), (failure_list, ["rs-fMRI.nii"])],
)
def test_list_func_scans(mock_acq_list, expected_list, mocker):
    walker = mocker.patch(
        "flywheel_bids.supporting_files.file_funcs.Walker", spec=Walker
    )
    acq = flywheel.Acquisition(label="test")
    acq.files = mock_acq_list
    walker.return_value.walk.return_value = (acq,)
    finals = list_func_scans(acq)
    final_list = [f.name for f in finals]
    assert final_list == expected_list


@pytest.mark.parametrize(
    "mock_acq_list, expected_list",
    [
        (success_list, ["the_sbref.nii.gz"]),
        (failure_list, ["the_failure_sbref.nii.gz"]),
    ],
)
def test_list_sbref_scans(mock_acq_list, expected_list, mocker):
    walker = mocker.patch(
        "flywheel_bids.supporting_files.file_funcs.Walker", spec=Walker
    )
    acq = flywheel.Acquisition(label="test")
    acq.files = mock_acq_list
    walker.return_value.walk.return_value = (acq,)
    finals = list_sbref_scans(Path(__file__).parents[1])
    final_list = [f.name for f in finals]
    assert final_list == expected_list
