# ruff: noqa: F841

import os
import shutil
import unittest
from unittest.mock import patch

import flywheel

from flywheel_bids import curate_bids
from flywheel_bids.supporting_files import project_tree, templates
from flywheel_bids.supporting_files.templates import (
    BIDS_V1_TEMPLATE_NAME,
    load_template,
)

from .utils_for_testing import check_for_fw_key

DEFAULT_TEMPLATE = load_template(None)


class BidsCurateTestCases(unittest.TestCase):
    def setUp(self):
        # Define testdir
        self.testdir = "testdir"

    def tearDown(self):
        # Cleanup testdir, if present
        if os.path.exists(self.testdir):
            shutil.rmtree(self.testdir)

    """The following tests make sure that the project curation template is valid and that
    specific examples of meta_info are valid according to that template.  meta_info simulates
    the result of BIDS template processing on a file."""

    def test_ValidateMetaInfo_Default_Valid(self):
        """Check that the template itself is valid."""
        meta_info = {"info": {"BIDS": {}}}
        curate_bids.validate_meta_info(meta_info, DEFAULT_TEMPLATE)
        # Assert 'BIDS.valid' == True
        self.assertTrue(meta_info["info"]["BIDS"]["valid"])
        # Assert error message is empty string
        self.assertEqual(meta_info["info"]["BIDS"]["error_message"], "")

    def test_ValidateMetaInfo_EmptyPath_Valid(self):
        """An empty Path means the file is in the top level BIDS folder."""
        meta_info = {"info": {"BIDS": {"Path": "", "extra": "test"}}}
        curate_bids.validate_meta_info(meta_info, DEFAULT_TEMPLATE)
        # Assert 'BIDS.valid' == True
        self.assertTrue(meta_info["info"]["BIDS"]["valid"])
        # Assert error message is empty string
        self.assertEqual(meta_info["info"]["BIDS"]["error_message"], "")

    def test_ValidateMetaInfo_EmptyFolder_Valid(self):
        """An empty Folder means the file is in the top level BIDS folder."""
        meta_info = {"info": {"BIDS": {"Folder": "", "extra": "test"}}}
        curate_bids.validate_meta_info(meta_info, DEFAULT_TEMPLATE)
        # Assert 'BIDS.valid' == True
        self.assertTrue(meta_info["info"]["BIDS"]["valid"])
        # Assert error message is empty string
        self.assertEqual(meta_info["info"]["BIDS"]["error_message"], "")

    def test_ValidateMetaInfo_NonRequired_Valid(self):
        """Non-required entities don't have to have a value."""
        meta_info = {
            "info": {"BIDS": {"Run": "", "Ce": "", "Mod": "", "Acq": "", "Rec": ""}}
        }
        curate_bids.validate_meta_info(meta_info, DEFAULT_TEMPLATE)
        # Assert 'BIDS.valid' == True
        self.assertTrue(meta_info["info"]["BIDS"]["valid"])
        # Assert error message is empty string
        self.assertEqual(meta_info["info"]["BIDS"]["error_message"], "")

    def test_ValidateMetaInfo_RunValueIsInteger_Valid(self):
        meta_info = {"info": {"BIDS": {"Run": 1}}}
        curate_bids.validate_meta_info(meta_info, DEFAULT_TEMPLATE)
        # Assert 'BIDS.valid' == True
        self.assertTrue(meta_info["info"]["BIDS"]["valid"])
        # Assert error message is empty string
        self.assertEqual(meta_info["info"]["BIDS"]["error_message"], "")
        # Assert run numebr is still an integer
        self.assertEqual(meta_info["info"]["BIDS"]["Run"], 1)

    def test_ValidateMetaInfo_MissingTask_Invalid(self):
        meta_info = {
            "info": {
                "BIDS": {
                    "template": "func_events_file",
                    "Filename": "example.tsv",
                    "Task": "",
                    "Modality": "bold",
                }
            }
        }
        curate_bids.validate_meta_info(meta_info, DEFAULT_TEMPLATE)
        # Assert 'BIDS.valid' == False
        self.assertFalse(meta_info["info"]["BIDS"]["valid"])
        # Assert error message is correct
        self.assertEqual(
            meta_info["info"]["BIDS"]["error_message"],
            "Task '' does not match '^[a-zA-Z0-9]+$'",
        )

    def test_ValidateMetaInfo_MissingFilename_Invalid(self):
        meta_info = {
            "info": {
                "BIDS": {"template": "anat_file", "Filename": "", "extra": "test"},
            }
        }
        curate_bids.validate_meta_info(meta_info, DEFAULT_TEMPLATE)
        # Assert 'BIDS.valid' == False
        self.assertFalse(meta_info["info"]["BIDS"]["valid"])
        # Assert error message is correct
        self.assertEqual(
            meta_info["info"]["BIDS"]["error_message"],
            "Filename '' should be non-empty\n'Suffix' is a required property",
        )

    def test_ValidateMetaInfo_MissingModality_Invalid(self):
        meta_info = {
            "info": {
                "BIDS": {
                    "template": "anat_file",
                    "Filename": "example.nii.gz",
                    "Suffix": "",
                }
            }
        }
        curate_bids.validate_meta_info(meta_info, DEFAULT_TEMPLATE)
        # Assert 'BIDS.valid' == False
        self.assertFalse(meta_info["info"]["BIDS"]["valid"])
        # Assert error message is correct
        self.assertEqual(
            meta_info["info"]["BIDS"]["error_message"],
            "Suffix '' is not one of ['T1w', 'T2w', 'T1rho', 'T1map', 'T2map', 'T2star', 'FLAIR', 'FLASH', 'PD', 'PDmap', 'PDT2', 'inplaneT1', 'inplaneT2', 'angio', 'defacemask']",
        )

    def test_ValidateMetaInfo_NoTemplateMatch_Invalid(self):
        """Not finding a matching definition should cause an error."""
        meta_info = {"info": {"BIDS": {"template": "NO_MATCH", "Modality": "bold"}}}
        curate_bids.validate_meta_info(meta_info, DEFAULT_TEMPLATE)
        # Assert 'BIDS.valid' == False
        self.assertFalse(meta_info["info"]["BIDS"]["valid"])
        # Assert error message is correct
        self.assertEqual(
            meta_info["info"]["BIDS"]["error_message"], "Unknown template: NO_MATCH. "
        )

    def test_ValidateMetaInfo_NoEchoMatch_Invalid(self):
        meta_info = {
            "info": {
                "BIDS": {
                    "template": "func_file",
                    "Filename": "example.nii.gz",
                    "Modality": "sbref",
                    "Task": "task",
                    "Rec": "",
                    "Run": "01",
                    "Echo": "AA",
                }
            }
        }
        curate_bids.validate_meta_info(meta_info, DEFAULT_TEMPLATE)
        # Assert 'BIDS.valid' == False
        self.assertFalse(meta_info["info"]["BIDS"]["valid"])
        # Assert error message is correct
        self.assertEqual(
            meta_info["info"]["BIDS"]["error_message"],
            "Echo 'AA' does not match '^[0-9]*$'\n'Suffix' is a required property",
        )

    def test_ValidateMetaInfo_NoPropertyDefinitionMatch_Invalid(self):
        meta_info = {
            "info": {
                "BIDS": {
                    "template": "anat_file",
                    "Modality": "invalid._#$*%",
                    "Ce": "invalid2.",
                    "Mod": "_invalid2",
                    "Filename": "",
                }
            }
        }
        curate_bids.validate_meta_info(meta_info, DEFAULT_TEMPLATE)
        # Assert 'BIDS.valid' == False
        self.assertFalse(meta_info["info"]["BIDS"]["valid"])
        # Assert error message is correct
        self.assertEqual(
            meta_info["info"]["BIDS"]["error_message"],
            "Filename '' should be non-empty\n'Suffix' is a required property\nMod '_invalid2' does not match '^[a-zA-Z0-9]*$'\nCe 'invalid2.' does not match '^[a-zA-Z0-9]*$'",
        )

    def test_ValidateMetaInfo_InfoDefinedNoBids_BidsIsNa(self):
        meta_info = {"info": {"test1": "abc", "test2": "def"}}
        curate_bids.validate_meta_info(meta_info, DEFAULT_TEMPLATE)
        # Assert 'BIDS': 'NA' is in meta_info
        self.assertEqual(meta_info["info"]["BIDS"], "NA")

    def test_ValidateMetaInfo_InfoDefinedYesBids_BidsIsNa(self):
        meta_info = {"info": {"BIDS": "NA"}}
        curate_bids.validate_meta_info(meta_info, DEFAULT_TEMPLATE)
        # Assert 'BIDS': 'NA' is in meta_info
        self.assertEqual(meta_info["info"]["BIDS"], "NA")

    def test_ValidateMetaInfo_NoInfoDefined_BidsIsNa(self):
        meta_info = {"other_info": "test"}
        curate_bids.validate_meta_info(meta_info, DEFAULT_TEMPLATE)
        # Assert 'BIDS': 'NA' is in meta_info
        self.assertEqual(meta_info["info"]["BIDS"], "NA")

    def test_ValidateMetaInfo_InfoDefined_BidsIsValid(self):
        meta_info = {
            "info": {
                "BIDS": {
                    "Filename": "sub-01_ses-02_T1w.nii.gz",
                    "Modality": "T1w",
                    "valid": True,
                    "error_message": "",
                }
            }
        }
        curate_bids.validate_meta_info(meta_info, DEFAULT_TEMPLATE)
        # Assert 'BIDS.valid' == True
        self.assertTrue(meta_info["info"]["BIDS"]["valid"])
        # Assert error message is empty string
        self.assertEqual(meta_info["info"]["BIDS"]["error_message"], "")

    def test_ValidateMetaInfo_InfoDefined_BidsIsInvalid(self):
        meta_info = {
            "info": {
                "BIDS": {
                    "Task": "testtask",
                    "Modality": "bold",
                    "Filename": "sub-01_ses-02_task-testtask_bold.nii.gz",
                    "valid": False,
                    "error_message": "Missing required property: Task. ",
                }
            }
        }
        curate_bids.validate_meta_info(meta_info, DEFAULT_TEMPLATE)
        # Assert 'BIDS.valid' == True
        self.assertTrue(meta_info["info"]["BIDS"]["valid"])
        # Assert error message is empty string
        self.assertEqual(meta_info["info"]["BIDS"]["error_message"], "")

    def test_ValidateMetaInfo_InfoDefined_BIDSv0Template(self):
        meta_info = {
            "info": {
                "BIDS": {
                    "Task": "testtask",
                    "Modality": "bold",
                    "Filename": "sub-01_ses-02_task-testtask_bold.nii.gz",
                    "valid": False,
                    "error_message": "Missing required property: Task. ",
                }
            }
        }
        bids_v1_template = load_template(None, template_name=BIDS_V1_TEMPLATE_NAME)
        curate_bids.validate_meta_info(meta_info, bids_v1_template)
        # Assert 'BIDS.valid' == True
        self.assertTrue(meta_info["info"]["BIDS"]["valid"])
        # Assert error message is empty string
        self.assertEqual(meta_info["info"]["BIDS"]["error_message"], "")

    def test_intended_for_metadata(self):
        project = project_tree.TreeNode("project", {"label": "testProj"})

        template = load_template(None, save_sidecar_as_metadata=True)

        count = curate_bids.Count()

        count = curate_bids.curate_bids_tree(
            None,
            template,
            project,
            count,
            reset=False,
            recurate_project=True,
            dry_run=False,
            save_sidecar_as_metadata=True,
        )

        self.assertEqual(count.containers, 1)
        self.assertEqual(count.files, 0)
        self.assertEqual(count.acquisitions, 0)
        self.assertEqual(count.sessions, 0)

        session = project_tree.TreeNode(
            "session", {"label": "session1", "subject": {"code": "subj1"}}
        )
        project.children.append(session)

        acq1 = project_tree.TreeNode(
            "acquisition",
            {
                "label": "fmap-epi_acq-1",
                "created": "2018-01-17T07:58:09.799Z",
                "id": "647a6379d31f8dddf80c5538",  # actual ID on latest.sse.flywheel.io
            },
        )
        session.children.append(acq1)
        file1 = project_tree.TreeNode(
            "file",
            {
                "name": "4_fmap_SE_PA.nii.gz",
                "type": "nifti",
                "classification": {"Intent": "Fieldmap"},
            },
        )
        acq1.children.append(file1)

        acq2 = project_tree.TreeNode(
            "acquisition",
            {"label": "func-bold_task-rest_run-1", "id": "no_stinking_badges"},
        )
        session.children.append(acq2)
        file2 = project_tree.TreeNode(
            "file",
            {
                "name": "task1.nii.gz",
                "type": "nifti",
                "modality": "MR",
                "classification": {"Intent": "Functional"},
            },
        )
        acq2.children.append(file2)

        # by passing None for fw, it won't really make the changes
        count = curate_bids.curate_bids_tree(
            None,
            template,
            project,
            count,
            reset=True,
            recurate_project=False,
            dry_run=False,
            save_sidecar_as_metadata=True,
        )

        self.assertIn("IntendedFor", file1["info"]["BIDS"]["sidecarChanges"])
        self.assertEqual(len(file1["info"]["BIDS"]["sidecarChanges"]["IntendedFor"]), 1)
        self.assertEqual(
            file1["info"]["BIDS"]["sidecarChanges"]["IntendedFor"][0],
            "ses-session1/func/sub-subj1_ses-session1_task-rest_run-1_bold.nii.gz",
        )
        self.assertEqual(
            count.containers, 4
        )  # = 1 project + 1 session + 2 acquisitions
        self.assertEqual(count.files, 2)
        self.assertEqual(count.acquisitions, 2)
        self.assertEqual(count.sessions, 1)

    def test_intended_for_sidecar(self):
        err = check_for_fw_key(["latest."])
        if err != "ok":
            unittest.TestCase.skipTest("", err)

        project = project_tree.TreeNode("project", {"label": "testProj"})

        # save_sidecar_as_metadata=False is the default and means sidecar data is in sidecar files
        # not metadata
        template = load_template(save_sidecar_as_metadata=False)

        count = curate_bids.Count()

        # Curate project all by itself (pass None for first argument "fw" so it won't try to update for real)
        count = curate_bids.curate_bids_tree(
            None,
            template,
            project,
            count,
            recurate_project=False,
            dry_run=False,
            save_sidecar_as_metadata=False,
        )

        session = project_tree.TreeNode(
            "session", {"label": "session1", "subject": {"code": "subj1"}}
        )
        project.children.append(session)

        # Actual acquisition label/ID on latest.sse.flywheel.io fw://bids-curation-test/Modify sidecar test
        # Use Flywheel viewer extension "Launch in Developer Utilities" to get the ID
        acq1 = project_tree.TreeNode(
            "acquisition",
            {
                "label": "4 - fmap-SE-PA",
                "created": "2023-06-02 17:47:37",
                "id": "647a6379d31f8dddf80c5538",
            },
        )
        session.children.append(acq1)
        file1 = project_tree.TreeNode(
            "file",
            {
                "name": "4_fmap_SE_PA.nii.gz",
                "type": "nifti",
                "classification": {"Intent": "Fieldmap"},
            },
        )
        acq1.children.append(file1)

        acq2 = project_tree.TreeNode(
            "acquisition",
            {"label": "func-bold_task-rest_run-1", "id": "no_stinking_badges"},
        )
        session.children.append(acq2)
        file2 = project_tree.TreeNode(
            "file",
            {
                "name": "task1.nii.gz",
                "type": "nifti",
                "modality": "MR",
                "classification": {"Intent": "Functional"},
            },
        )
        acq2.children.append(file2)

        count = curate_bids.curate_bids_tree(
            None,
            template,
            project,
            count,
            recurate_project=True,
            dry_run=False,
            save_sidecar_as_metadata=False,
        )

        self.assertEqual(
            file1["info"]["BIDS"]["sidecarChanges"]["IntendedFor"][0],
            "ses-session1/func/sub-subj1_ses-session1_task-rest_run-1_bold.nii.gz",
        )
        self.assertEqual(file2["info"]["BIDS"]["sidecarChanges"]["TaskName"], "rest")

    def test_project_files_created(self):
        err = check_for_fw_key(["latest."])
        if err != "ok":
            unittest.TestCase.skipTest("", err)

        fw = flywheel.Client()

        template = load_template(None, "reproin", False)

        project = fw.projects.find_one(
            "group=bids-curation-test,label=Modify sidecar test"
        )

        SIDECAR_NAME = "dataset_description.json"
        README_NAME = "README.txt"

        # First delete project files
        proj_files = [
            file.name
            for file in project.files
            if file.name in [SIDECAR_NAME, README_NAME]
        ]
        for file_name in proj_files:
            project.delete_file(file_name)

        if "BIDS" in project.info:
            del project.info["BIDS"]
            fw.replace_project_info(project.id, project.info)

        # get project again to update files
        project = fw.projects.find_one(
            "group=bids-curation-test,label=Modify sidecar test"
        )

        # Save sidecar but no data in project.info.BIDS
        curate_bids.save_project_files(project, template, False)

        # get project again to update files
        project = fw.projects.find_one(
            "group=bids-curation-test,label=Modify sidecar test"
        )

        proj_files = [
            file for file in project.files if file.name in [SIDECAR_NAME, README_NAME]
        ]
        self.assertEqual(len(proj_files), 2)

        self.assertFalse("BIDS" in project.info)

        created = proj_files[0].created

        # try to create the sidecar again, it should just log that it exists and not re-create it
        # also add data_description.json info to project.info
        curate_bids.save_project_files(project, template, True)

        # get project again to update files
        project = fw.projects.find_one(
            "group=bids-curation-test,label=Modify sidecar test"
        )

        proj_files = [
            file for file in project.files if file.name in [SIDECAR_NAME, README_NAME]
        ]
        self.assertEqual(len(proj_files), 2)
        self.assertEqual(
            proj_files[0].created, created
        )  # means file was not re-created

        self.assertTrue("project" in template.definitions)
        found_project_rule = False
        for rule in template.rules:
            if rule.template == "project":
                found_project_rule = True
        self.assertTrue(found_project_rule)

    @patch("flywheel.Project.upload_file")
    def test_old_project_file_works(self, mock_upload_file):
        """When storing sidecar data in metadata, the 'project' definition had the contents of the
        file dataset_description.json.  That is how templates used to be.  Now templates have that
        information in the definition of 'dataset_description'.  In order to be able to use old
        templates without modification, a check for the 'project' definition is made and if found,
        it is used instead of assuming that information needs to be copied from 'dataset_description'"""

        project = flywheel.Project("testProj")
        project.files = []

        data = {
            "namespace": "BIDS",
            "definitions": {
                "project": {
                    "properties": {
                        "Acknowledgements": {
                            "type": "string",
                            "title": "Acknowledgements",
                            "default": "",
                        },
                    },
                    "required": [],
                },
            },
            "description": "Simulate an old style template",
        }
        template = templates.Template(data, save_sidecar_as_metadata=False)

        curate_bids.save_project_files(project, template, True)

        assert mock_upload_file.call_count == 2


if __name__ == "__main__":
    unittest.main()
