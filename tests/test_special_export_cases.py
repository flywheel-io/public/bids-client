"""Test for when a json file in FW does not contain BIDS info

To make sure the specific acquisition (and files) are not deleted from the instance,
 we ship it in the assets folder"""

import logging
import unittest
from os import walk
from pathlib import Path

import pytest

from tests import test_integration_bids_workflow
from tests.utils_for_testing import check_for_fw_key

assets_path = Path(__file__).parent / "assets"

API_KEY = None
GROUP_ID = "bids-client"
GROUP_LABEL = "BIDS Integration Testing"
PROJECT_LABEL = "bids-client-export"

log = logging.getLogger(__name__)
logging.basicConfig(level=logging.DEBUG)


def count_files(filepath):
    file_list = []
    for _, _, files in walk(filepath):
        for f in files:
            file_list.append(f)
    return len(file_list)


@pytest.mark.parametrize(
    "special_case",
    [
        "json_no_bids",
        "new_sequences/asl",
        "site-specific/bids_style",
        "site-specific/reproin_style",
    ],
)
# @unittest.skip("Integration test")
def test_special_exports_succeed(special_case, tmpdir):
    """Check that export_bids even when a json doesn't have BIDS metadata
    or has new ASL pieces."""
    from _pytest.monkeypatch import MonkeyPatch

    MonkeyPatch().setattr(
        "flywheel_bids.supporting_files.utils.confirmation_prompt", lambda x: "no"
    )
    # check for API key; if not found, it skips this test:
    err = check_for_fw_key(["latest.", "rollout.sse"])
    if err != "ok":
        unittest.TestCase.skipTest("", err)

    bids_dataset_dir = Path(assets_path / special_case)
    log.info("Uploading bids project to instance.")
    # NOTE: If error is received about project label existing, check if you
    # have permission for the test project.
    if API_KEY:
        is_imported = test_integration_bids_workflow.run_flywheel_bids_import(
            api_key=API_KEY,
            group_id=GROUP_ID,
            group_label=GROUP_LABEL,
            project_label=PROJECT_LABEL,
            bids_dataset_dir=bids_dataset_dir,
            overwrite=True,
        )

        if is_imported:
            log.info("Exporting bids.")
            is_exported = test_integration_bids_workflow.run_flywheel_bids_export(
                api_key=API_KEY,
                group_id=GROUP_ID,
                project_label=PROJECT_LABEL,
                output_bids_dataset_dir=tmpdir,
            )
        assert is_exported
