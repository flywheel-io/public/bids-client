from unittest.mock import patch

import pytest

from flywheel_bids.supporting_files.bidsify_flywheel import (
    create_match_info_update,
    rule_matches,
)


@patch(
    "flywheel_bids.supporting_files.bidsify_flywheel.add_properties",
    return_value="mocked_values",
)
def test_create_match_info_update(mock_add_properties, mock_rule):
    mock_context = {"container_type": "file"}
    mock_container = {"classification": "asl"}
    result_match_info = create_match_info_update(
        mock_rule, mock_context, mock_container, "template_properties", "test_namespace"
    )
    expected_match_info = {
        "template": "test_curation_template",
        "rule_id": "passport",
        "ignore": False,
    }
    expected_container = mock_container
    expected_container.update({"info": {"test_namespace": "mocked_values"}})
    assert mock_add_properties.called_once()
    assert result_match_info == expected_match_info
    assert mock_container == expected_container


@pytest.mark.parametrize(
    "rule_match_value,expected", [(True, "matches"), (False, "not matched")]
)
def test_rule_matches(rule_match_value, expected, mock_rule, mock_context, caplog):
    with patch(
        "flywheel_bids.supporting_files.bidsify_flywheel.templates.Rule.test",
        return_value=rule_match_value,
    ):
        result = rule_matches(mock_rule, mock_context, "gobblety gook")

    assert rule_match_value == result
