# Frequently Asked Questions (FAQ)

- Does the template need to be updated to include my new sequence for it to be
  uploaded?
  - Yes and no, the template is checked during `upload_bids.upload_bids_dir`
  - Data is uploaded using the default template. If the data is BIDS-compliant,
    further templating may not be necessary.
  - If the filenames are in reproin or other conventions, curation may require
    additional information in the template.
- Can I just test how a file will be uploaded?

  - Yes, use this
    [jupyter notebook](../quick_checker/name_conversion_checker.ipynb) that
    draws on `upload_bids.determine_acquisition_label`

- What happens if there is only one session and it does not have a session
  label?

  - Flywheel will use "ses-" as the session label and handle all the exporting
    as though there was no session directory.

- How do I "upgrade" an older, curated project to the default curation style,
  where sidecars are present in each acquisition next to the NIfTI file?
  - To standardize how a project is curated, follow this
    [Jupyter notebook](https://gitlab.com/flywheel-io/scientific-solutions/tutorials/notebooks/bids-sidecar-standardization/-/blob/main/BIDS_sidecar_hierarchy_curator.ipynb)
