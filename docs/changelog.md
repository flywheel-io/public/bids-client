# Changes

## 1.2.27

ENH:

- Added docs/deciphering_rules.md as a living doc with information to decrease
  dev time learning and debugging BIDS rule failures
- Introduced a `silent_logger` system to capture curation issues, so that it is
  easier to sift through the gear output to determine which files failed and
  where they failed. This logging output should decrease the need to
  step-through rules significantly.

## 1.1.0

- Note helpful general
  [BIDS help](https://bids-standard.github.io/bids-starter-kit/folders_and_files/folders.html)
- For examples to upload for testing, consider using this
  [repo](https://github.com/bids-standard/bids-examples)
