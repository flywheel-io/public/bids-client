<!-- markdownlint-disable MD024 -->

# Release Notes

## 1.2.25

### Bug fix

- Small fix for reproin_fieldmap_file definition in reproin.json. Add Part to
  definition, as it was previously missing.

## 1.2.24

### Bug fix

- Small fix for reproin_diffusion_file rule in reproin.json. The acquisition
  label regex, Measurement and Intent matches are now outside of the $or so they
  can be used for NIfTIs as well as .json .bval and .bvec.

## 1.2.23

### Change

- Improve detection of how curated (sidecar data in sidecars vs in NIfTI file
  metadata).

### Enhancement

- add flywheel_bids_app_toolkit/autoupdate.py

### Bug fix

- fix $or processing, added tests
- reproin.json add $or to allow dwi files to interrogate file.info.header.dicom
  or file.info

## 1.2.22

### Bug fix

- Take out autoincrement of run counters

## 1.2.21

### Change

### Enhancement

### Bug fix

- Temporary fix for force reloading session in `project_tree.py:set_tree` so
  that `session["info"]` is complete.

## 1.2.20

### Change

### Enhancement

### Bug fix

- Fix run counter for remaining `reproin` file types, so all files associated
  with a single scan are assigned same run number

## 1.2.19

### Change

### Enhancement

### Bug fix

- Fix bug so that BIDS custom metadata object is only updated on files where it
  exists.

## 1.2.18

### Change

### Enhancement

### Bug fix

- Fix run counter for `anat` files, so `.nii.gz` and `.json` file pairs are
  assigned same run number

## 1.1.6

### Change

- BIDS sidecar information is no longer stored on in `file.info`. Refer to the
  .json sidecar for values
- BIDS sidecar is downloaded directly from the acquisition's .json sidecar and
  not the metadata

### Enhancement

- Warn users if trying to import a modality that is unsupported

### Bug fix

- Correct placement of "require" section in perf_tsv_file definition

## 1.1.0

### Enhancement

- Added ASL functionality
- Upgraded to bids-validator 1.9.9
- Improve search patterns for file classification on upload

### Docs

- Added changelog.md, FAQ.md, quick_checker folder with ipynbs for easier dev

### Testing

- Add tests for ASL and new classification functions

## 1.0.9

- Cast to string so regexes won't fail on numbers
- Updated Poetry, fixed Dockerfile

## 1.0.8

- Updated CI
- Moved BIDS Study Design.xlsx to a Google Doc
- Added list handling in the matching section of initializers/template rules
  added test for the future fixed an issue with existing tests, would get FAILED
  ../../../src/tests/test_export_json_no_bids.py::test_export_json_no_bids
- OSError: pytest: reading from stdin while output is captured! Consider using
  '-s'. Now mock the call that was waiting for input capture.
- Added ability to test for the presence or absence of a DICOM tag, not just a
  particular value

## 1.0.5 --> 1.0.7

See <https://gitlab.com/flywheel-io/public/bids-client/-/releases/1.0.5>

## 1.0.4

### Bug fix

- Exported filenames were using the original filenames, rather than the
  BIDSified filename.

## 1.0.3

### Docs

- Add release notes

### Testing

- Expand the coverage of test_integration_bids_workflow to include re-import and
  re-curate steps to test ReproIn export methods (and ReproIn import methods)
- Test complex logic methods

### ENH

- Upload json's to the project level, if present. This change is aimed at
  including task and sequence parameters.
- Add a semi-hidden overwrite toggle for upload_bids
- Allow BIDS sidecar information to be uploaded and BIDSified
- Introduce complex logic for rules (combining $or/$and/$not/etc)
