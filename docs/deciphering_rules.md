# How to decipher templated rules for curation

[Where to step in to debug a rule](#debug-hint)

[Where to find more detailed explanations and instructions](https://docs.flywheel.io/Developer_Guides/dev_bids_template_file/#introduction)

## Explanation of BIDS Project Curation Templates

The `reproin.json` file is an example project curation template used for curating and
organizing neuroimaging data according to the BIDS (Brain Imaging Data Structure)
standard. This file contains rules and definitions that help map the original data to
the BIDS format. Here is a breakdown of the key components:

### Key Components

1. **Namespace**:

   - The `namespace` field defines the scope or context for the rules and definitions
     within the template. It helps in organizing and applying the rules to the
     appropriate data. For BIDS, the namespace is always "BIDS".

2. **Definitions**: The `definitions` section provides the structure and metadata that
   should be applied to the data. It includes fields like BIDS entities, which define
   how the data should be named and organized according to the BIDS standard. - First,
   defaults and patterns are given for each simple BIDS entity (i.e., "Acq"). Following
   these are Flywheel containers (i.e., "project", "subject", etc.) and then types of
   images collected (i.e., "anat_file", "func_file", etc.).

   - Fields in the `properties` section like `Filename`, `Folder`, and `Path` are
     metadata that are to be populated.
   - The `auto_update` component field within `properties` supplies the expected
     formatting for a finished/curated BIDS file.
   - The other entity component fields (e.g., "Mod") set up what to look for when
     splicing together the original names into a BIDSified name.

3. **Rules**:

   - The `rules` section contains rules that are based on the definitions. Rules contain
     "where" and "initialize" clauses that determin if the rule applies to the current
     context, and if so, what information to extract. Here "context" refers to a
     particular container or file in the Flywheel hierarchy: group, project, subject,
     session, acquisition or a file attached to any container. The process of curation
     involves walking the Flywheel hierarchy top-down to set the BIDS metadata for each
     container or file.

4. **Where Clause**:

   - The `where` clause specifies conditions that must be met for the rules to be
     applied. It includes component fields such as `file.classification`,
     `acquisition.label`, and conditions like `$in` and `$regex`. These conditions help
     in identifying the relevant data that needs to be curated.

5. **Initialize Clause**:

   - The `initialize` clause specifies how the BIDS entities like "Acq", "Run" and
     "Suffix" get set based on information from the current context.

## High level anatomy of the JSON

The Definitions section is at the top and "defines" how a container is handled, if the
container is found to match a Rule in the lower portion of the JSON.

- When creating or modifying a rule, you must choose a definition for the rule to
  follow. For example, a container matching the "reproin_perf_file" rule must follow the
  "perf_file" definition.

## Key methods in curation

[bidsify_flywheel.process_matching_templates](../flywheel_bids/supporting_files/bidsify_flywheel.py#121)

- The heart of the matching scheme
- Rules are matched and used in
  [`bidsify_flywheel.process_matching_templates`](../flywheel_bids/supporting_files/bidsify_flywheel.py#217).
  `rule.template` is the "template" field of the rule being matched (e.g., "perf_file").
- The criteria for the rule are loaded into `templateDef` and checked sequentially.

- If a rule potentially matches, the templated definitions are loaded into a dict to
  populate for BIDS.info (`add_properties` called from `create_match_info_update`)

- All the rule criteria are then checked by `template.apply_custom_initialization`

### Debug hint

**STEP INTO
[`templates.Template.apply_custom_initialization.apply_initializers`](../flywheel_bids/supporting_files/templates.py#184)
to debug**

[templates.apply_initializers](../flywheel_bids/supporting_files/templates.py#L248)
02.2025 - Now heavily annotated

- For the `where` part of any rule in a template, there are many component fields (i.e.,
  "file.classification", "acquisition.label", or similar).
- There are different conditions or criteria in each of these component fields (i.e.,
  "$in", "$regex").
- `apply_initializers` attempts to match up pieces/BIDS entities in the original name or
  info blob of the file being curated against the component fields in the rule. This
  match occurs so the name and/or metadata associated with the file can be updated to
  satisfy the BIDS naming conventions.
