FROM flywheel/python:3.11-debian AS base
LABEL maintainer="Flywheel <support@flywheel.io>"
SHELL ["/bin/bash", "-euxo", "pipefail", "-c"]
ENV FLYWHEEL=/var/flywheel/code
WORKDIR $FLYWHEEL
COPY requirements.txt ./
RUN apt-get -yq update && \
    apt-get -yq install --no-install-recommends npm jq && \
    apt-get -yq clean && \
    rm -rf /var/lib/apt/lists/*
# https://www.npmjs.com/package/bids-validator
RUN npm install -g bids-validator@1.14.0

FROM base AS dev
COPY requirements-dev.txt ./
RUN uv pip install -rrequirements-dev.txt
COPY . .
RUN uv pip install --no-deps -e.

FROM base AS prod
COPY . .
RUN uv pip install --no-deps -e.
USER flywheel
